clear all
close all

%% Risolviamo il problema
%%
%%  x'' = -2 x (2 x^2 - 1)
%%
%% con un metodo RK di tableau
%%
%%   0  |
%%  1/3 |  1/3
%%  2/3 | -1/3   1
%%   1  |   1   -1   1
%%  ----+-----------------
%%      |  1/8  3/8 3/8 1/8

%% Dati
% Dati del problema
t0 = 0;
tf = 3;
fun = @(y) [y(2); -2 * y(1) * (2 * y(1)^2 - 1)];
y0 = [0; 2];

% Dati del metodo
A = [ 0,   0, 0, 0;
     1/3,  0, 0, 0;
     -1/3, 1, 0, 0;
       1, -1, 1, 0];
c = sum (A, 1);
b = [1, 3, 3, 1] / 8;
s = length (b); % numero di stati
K = zeros(2, s);

%% Soluzione di riferimento
mrif = 2 ^ 12 + 1;
k = (tf - t0) / (mrif - 1);
Y = zeros(2, mrif);
Y(:, 1) = y0;
for n = 1:mrif-1
	for idx = 1:s
		K(:, idx) = fun(Y(:, n) + k * K * A(idx, :) .');
	end
	Y(:, n+1) = Y(:, n) + k * K * b .';
end

y_rif = Y(:, n+1);

[X, V] = meshgrid (linspace(-3,3, 50), linspace(-3,3, 50));
F1 = V;
F2 = -2 * X .* (2 * X .^ 2 - 1);
% Normalizziamo il campo vettoriale per il plot
F1 = F1 ./ sqrt (F1 .^ 2 + F2 .^ 2);
F2 = F2 ./ sqrt (F1 .^ 2 + F2 .^ 2);

figure
hold on
quiver(X, V, F1, F2)
plot(Y(1, :), Y(2, :), '-r', 'LineWidth', 2)
axis([-3, 3, -3, 3])
xlabel('x')
ylabel('v')

t_span = linspace (t0, tf, mrif);
figure
plot(t_span, Y(1,:), t_span, Y(2,:))
xlabel('t')
legend('x', 'v')

%% Controllo della convergenza del metodo
mrange = 2 .^ (4:10) + 1;
err = zeros (1, length (mrange));
count = 0;
for m = mrange
	count = count + 1;
	k = (tf - t0) / (m - 1);
	y = y0;
	for n = 1:m-1
		for idx = 1:s
			K(:, idx) = fun(y + k * K * A(idx, :) .');
		end
		y = y + k * K * b.';
	end
	err(count) = norm (y - y_rif, inf);
end

ord1 = err(1) * (mrange / mrange(1)) .^ (-1);
ord2 = err(1) * (mrange / mrange(1)) .^ (-2);
ord3 = err(1) * (mrange / mrange(1)) .^ (-3);
ord4 = err(1) * (mrange / mrange(1)) .^ (-4);
ord5 = err(1) * (mrange / mrange(1)) .^ (-5);
figure
loglog (mrange, ord1, mrange, ord2, mrange, ord3, mrange, ord4, mrange, ord5, mrange, err, 'xk')
xlabel('m')
legend('ord1', 'ord2', 'ord3', 'ord4', 'ord5', 'err')
