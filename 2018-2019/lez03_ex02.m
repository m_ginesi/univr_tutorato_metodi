clear all
close all

%% Risolvo il problema
%%
%% u'' + sin(u') + u = x  x \in (0,1)
%% u(0) = 0
%% u(1) = 1
%%
%% confrontando l'errore rispetto ad una soluzione di riferimento

%% Dati
maxit = 100;
tol = 1e-12;
mrange = 2 .^ (4:10) + 1;
M = 2 ^ 12 + 1;
err = zeros (1, length (mrange));
count = 0;

%% Soluzione di riferimento

X = linspace (0, 1, M).';
H = 1 / (M - 1);
D1 = toeplitz (sparse (1, 2, -1/2/H, 1, M), sparse (1, 2, 1/2/H, 1, M));
D2 = toeplitz (sparse ([1,1], [1,2], [-2,1]/H/H, 1, M));
A = D2 + speye (M);
A(1, 1:2) = [1, 0];
A(M, M-1:M) = [0, 1];
b = X;
b(1) = 0;
b(M) = 1;
DD1 = D1(2:M-1, :);
% Scriviamo la funzione che vogliamo azzerare ...
Fun = @(u) A * u + [0; sin(DD1*u); 0] - b;
% ... ed il suo Jacobiano
Jfun = @(u) A + spdiags ([[-cos((u(3:M) - u(1:M-2))/2/H); 0; 0],...
	[0; 0; cos((u(3:M) - u(1:M-2))/2/H)]]/2/H, [-1, 1], M, M);
% Applichiamo il metodo di Newton
it = 0;
U = linspace (0, 1, M).';
res = -Jfun (U) \ Fun (U);
while (it < maxit && norm(res) > tol)
	it = it + 1;
	U = U + res;
	res = -Jfun (U) \ Fun (U);
end
U = U + res; % <- sol. di riferimento

%% Ciclo su mrange
for m = mrange
	count = count + 1;
	x = linspace (0, 1, m).';
	h = 1 / (m - 1);
	D1 = toeplitz (sparse (1, 2, -1/2/h, 1, m), sparse (1, 2, 1/2/h, 1, m));
	D2 = toeplitz (sparse ([1,1], [1,2], [-2,1]/h/h, 1, m));
	A = D2 + speye (m);
	A(1, 1:2) = [1, 0];
	A(m, m-1:m) = [0, 1];
	b = x;
	b(1) = 0;
	b(m) = 1;
	DD1 = D1(2:m-1, :);
	% Scriviamo la funzione che vogliamo azzerare ...
	Fun = @(u) A * u + [0; sin(DD1*u); 0] - b;
	% ... ed il suo Jacobiano
	Jfun = @(u) A + spdiags ([[-cos((u(3:m) - u(1:m-2))/2/h); 0; 0],...
		[0; 0; cos((u(3:m) - u(1:m-2))/2/h)]]/2/h, [-1, 1], m, m);
	% Applichiamo il metodo di Newton
	it = 0;
	u = linspace (0, 1, m).';
	res = -Jfun (u) \ Fun (u);
	while (it < maxit && norm(res) > tol)
		it = it + 1;
		u = u + res;
		res = -Jfun (u) \ Fun (u);
	end
	u = u + res;
	urif = U(1:(M-1)/(m-1):M);
	err(count) = norm (u - urif, inf);
end

ord2 = err(1) * (mrange / mrange(1)) .^ (-2);

figure
loglog (mrange, ord2, '-', mrange, err, 'x')
title ('Andamento dell errore')
legend('ordine 2','effettivo')
xlabel ('m')

figure
plot (X , U)
xlabel ('x')
title ('Soluzione di riferimento')
