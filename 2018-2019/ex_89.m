clear all
close all

%% Esercizio 89 temi d'esame

xl = - 1;
xr = 1;
tol = 1e-10;
maxit = 100;

%% Soluzione di riferimento
m = 2 ^ 10 + 1;
x = linspace (xl, xr, m).';
h = (xr - xl) / (m - 1);
d_m = [exp(x(2:m)); 0];
d_c = - 2 * exp (x);
d_p = [0; exp(x(1:m-1))];
exp_D2 = spdiags ([[d_m], [d_c], [d_p]], [-1, 0, 1], m, m) / h / h;
D1 = toeplitz (sparse (1, 2, -1, 1, m), sparse (1, 2, 1, 1, m)) / 2 / h;
A = exp_D2 + D1 + speye (m);
A(1,1:2) = [1, 0];
A(m, m-1:m) = [0, 1];
b = [2; ones(m - 2, 1); 2];
Fun = @(u) A * u - [0; u(2:m-1).^2; 0] - b;
JFun = @(u) A - spdiags([0; 2*(u(2:m-1)); 0], 0, m, m);
% Risoluzione
it = 0;
u = 2 * ones (m, 1);
res = -JFun (u) \ Fun (u);
while (it < maxit && norm (res) > tol)
	it = it + 1;
	u = u + res;
	res = -JFun (u) \ Fun (u);
end
u = u + res;
u_rif = u;
m_rif = m;

figure
plot (x, u)
title ('Soluzione di riferimento')

%% Convergenza del metodo
mrange = 2 .^ (4:8) + 1;
count = 0;
err = zeros (1, length (mrange));
for m = mrange
	count = count + 1;
	x = linspace (xl, xr, m).';
	h = (xr - xl) / (m - 1);
	d_m = [exp(x(2:m)); 0];
	d_c = - 2 * exp (x);
	d_p = [0; exp(x(1:m-1))];
	exp_D2 = spdiags ([[d_m], [d_c], [d_p]], [-1, 0, 1], m, m) / h / h;
	D1 = toeplitz (sparse (1, 2, -1, 1, m), sparse (1, 2, 1, 1, m)) / 2 / h;
	A = exp_D2 + D1 + speye (m);
	A(1,1:2) = [1, 0];
	A(m, m-1:m) = [0, 1];
	b = [2; ones(m - 2, 1); 2];
	Fun = @(u) A * u - [0; u(2:m-1).^2; 0] - b;
	JFun = @(u) A - spdiags([0; 2*(u(2:m-1)); 0], 0, m, m);
	% Risoluzione
	it = 0;
	u = 2 * ones (m, 1);
	res = -JFun (u) \ Fun (u);
	while (it < maxit && norm (res) > tol)
		it = it + 1;
		u = u + res;
		res = -JFun (u) \ Fun (u);
	end
	u = u + res;
	err(count) = norm (u - u_rif(1:(m_rif - 1)/(m - 1): m_rif), inf);
end

ord2 = err(1) * (mrange / mrange(1)) .^ (-2);

figure
loglog (mrange, ord2, mrange, err, 'x')
