clear all
close all

%% Risolviamo il problema
%%
%% u u' + u'' / 2 = 0  x \in (1,2)
%% u(1) = 1
%% u'(2) = -1/4
%%
%% la cui soluzione esatta e`
%% 1/x

%% Dati
mrange = 2 .^ (4:10) + 1;
count = 0;
tol = 1e-12;
maxit = 100;
err = zeros (1, length (mrange));

%% Ciclo su m
for m = mrange
	count = count + 1;
	x = linspace (1, 2, m).';
	h = 1 / (m-1);
	D1 = toeplitz (sparse (1,2,-1/2/h,1,m), sparse(1,2,1/2/h,1,m));
	D2 = toeplitz (sparse([1,1], [1,2], [-2,1]/h/h, 1, m));
	b = sparse ([1,m],[1,1],[1,1/4/h],m,1);
	D2(1,1:2) = [1,0] * 2;
	D2(m,m-1:m) = [1/h/h, -1/4-1/h/h] * 2;
	DD1 = D1(2:m-1,:);
	% Scriviamo la funzione da azzerare ...
	Fun = @(u) [0; (DD1*u).*u(2:m-1); 0] + 1/2 * D2 * u - b;
	% ... ed il suo Jacobiano
	Jfun = @(u) spdiags([[-u(2:m-1)/2/h;0;0],...
		[0; (u(3:m) - u(1:m-2))/2/h; 0],...
		[0; 0; u(2:m-1)/2/h]], [-1, 0, 1], m, m) + 1/2 * D2;
	% Metodo di Newton
	it = 0;
	u = ones (m,1);
	res = -Jfun (u) \ Fun (u);
	while (norm(res) > tol && it < maxit)
		u = u + res;
		res = -Jfun (u) \ Fun (u);
		it = it + 1;
	end
	u = u + res;
	% Calcoliamo l'errore
	usol = x .^ (-1);
	err(count) = norm (u- usol, inf);
end

ord2 = err(1) * (mrange/ mrange(1)) .^ (-2);
loglog (mrange, ord2, '-', mrange, err,'x')
