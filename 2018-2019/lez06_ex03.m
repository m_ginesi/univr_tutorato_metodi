clear all
close all

%% Risolviamo il problema
%%
%% x' = x^3 + x*y^2 - x - y
%% y' = y^3 + y*x^2 + x - y
%%
%% Usando il theta metodo

%% Dati
tstar = 5;
m = 2 ^ 12 + 1;
k = tstar / (m - 1);
theta = 1/3;
y0 = [1; 1] / 2;
% Parametri per il metodo di Newton
maxit = 100;
tol = 1e-10;

% Funzione e Jacobiano del problema
fun = @(x) [x(1) ^ 3 + x(2) ^ 2 * x(1) - x(1) - x(2);
            x(2) ^ 3 + x(1) ^ 2 * x(2) + x(1) - x(2)];
jfun = @(x) [3 * x(1) ^ 2 + x(2) ^ 2 - 1, 2 * x(2) * x(1) - 1;
             2 * x(1) * x(2) + 1,         3 * x(2) ^ 2 + x(1) ^ 2 - 1];

Fun = @(y, yn) y - yn -  k * (1 - theta) * fun (yn) - k * theta * fun (y);
JFun = @(y) eye(2) - k * theta * jfun (y);

%% Risolviamo il problema
Y = zeros (2, m);
Y(:,1) = y0;
for n = 1:m-1
	% Metodo di Newton
	it = 0;
	yn = Y(:, n); % soluzione al tempo precendente
	y = Y(:, n); % guess iniziale per la soluzione al tempo successivo
	res = -JFun (y) \ Fun (y, yn);
	while (it < maxit && norm(res)>tol)
		it = it + 1;
		y = y + res;
		res = -JFun (y) \ Fun (y, yn);
	end
	n;
	y = y + res;
	Y(:, n + 1) = y;
end
t = linspace(0, tstar, m);
figure
plot(t, Y(1,:), t, Y(2,:))
xlabel ('t')
legend('x_1', 'x_2')
title('Time evolution')

figure
plot(Y(1,:), Y(2,:))
xlabel ('x_1')
ylabel ('x_2')
title('Evolution')

[x, y] = meshgrid (linspace (-1,1,20)*2/3, linspace (-1,1,20)*2/3);
figure
hold on
quiver (x, y, x.^3 + y.^2.*x - x - y, y.^3 + x.^2.*y + x - y);
plot(Y(1,:), Y(2,:),'r')
title ('Vector field of f')
