clear all
close all

%% Risolviamo il problema lineare
%%
%% u_x - u_{xx} = (x+1) / (x^2)
%% u(1) = 0
%% u(e) = 1
%%
%% la cui soluzione esatta e`
%%
%% u(x) = log(x)

%% Dati
xl = 1; % estremo sinistro del dominio
xr = exp(1); % estremo destro del dominio
u_a = 0; % condizione al bordo sinistro
u_b = 1; % condizione al bordo destro
mrange = 2 .^ (4:10) + 1; % insieme del numero di passi di discretizzazione
count = 0; % inizializziamo il counter
err = zeros (1, length (mrange)); % inizializziamo il vettore degli errori

%% Ciclo sul numero di passi di discretizzazione
for m = mrange
	count = count + 1; % incrementiamo il counter
	x = linspace (xl, xr, m).'; % dominio
	h = (xr - xl) / (m - 1); % passo di discretizzazione
	% Definiamo le matrici di discretizzazione delle derivate prime e seconde
	D1 = toeplitz (sparse (1, 2, -1/2/h, 1, m), sparse (1, 2, 1/2/h, 1, m));
	D2 = toeplitz (sparse ([1, 1], [1, 2], [-2, 1]/h/h, 1, m));
	% Scriviamo la matrice del problema
	A = D1 - D2;
	% ed il termine noto
	b = (x + 1) ./ (x .^ 2);
	% Poniamo le condizioni al contorno
	A(1, 1:2) = [1, 0];
	A(m, m-1:m) = [0, 1];
	b(1) = u_a;
	b(m) = u_b;
	% Risolviamo il problema
	u = A \ b;
	% Calcoliamo l'errore
	u_sol = log (x);
	err(count) = norm (u - u_sol, inf);
end

%% Plot
ord = err(1) * (mrange / mrange(1)) .^ (-2); % andamento dell'errore atteso
figure
loglog (mrange, ord, '-', mrange, err, 'x')
legend ('errore atteso', 'errore effettivo')
xlabel ('m')
