clear all
close all

%% Risolviamo il problema
%%
%%  x_1 ' = x_2
%%  x_2 ' = - x_1 + x_2 - x_1 ^ 2 + x_2
%%
%% Utilizzando il metodo RK semi implicito
%%
%%  1/2 |  1/2
%%  2/3 |  1/6  1/2
%%  1/2 | -1/2  1/2  1/2
%%   1  |  3/2 -3/2  1/2  1/2
%%  ----+---------------------
%%      |  3/2 -3/2  1/2  1/2

%% Dati
% Dati del problema
tstar = 7;
fun = @(x) [x(2);
            -x(1) + x(2) - x(1)^2 * x(2)];
Jfun = @(x) [0,                1;
             -1 - 2*x(1)*x(2), 1 - x(1)^2];
x0 = [2; -2];

% Dati del metodo
A = [ 1/2,   0 ,  0,   0;
      1/6,  1/2,  0,   0;
     -1/2,  1/2, 1/2,  0;
      3/2, -3/2, 1/2, 1/2];
c = sum (A, 2);
b = [3, -3, 1, 1] / 2;
s = length (b); % numero di stadi
Fun = @(xn, K, k, ii) K(:, ii) - fun (xn + k * K * A(ii, :).');
JFun = @(xn, K, k, ii) eye (2) - k * A(ii, ii) * Jfun (xn + k * K * A(ii, :).');
maxit = 100;
tol = 1e-14;

%% Soluzione di riferimento
m = 2 ^ 10 + 1;
k = tstar / (m - 1);
X = zeros (2, m);
X(:, 1) = x0;
K = zeros (2, s); % inizializziamo la matrice dei coefficienti
for n = 1:m-1
	xn = X(:, n);
	% Calcoliamo i coefficienti K_i
	for ii = 1:s
		res = - JFun (xn, K, k, ii) \ Fun (xn, K, k, ii);
		it = 0;
		while ((it < maxit) && (norm (res) > tol))
			K(:, ii) = K(:, ii) + res;
			it = it + 1;
			res = - JFun (xn, K, k, ii) \ Fun (xn, K, k, ii);
		end
		K(:, ii) = K(:, ii) + res;
	end
	X(:, n + 1) = xn + k * K * b.';
end

figure
plot(X(1, :), X(2, :))
xlabel('x')
ylabel('y')

t_span = linspace (0, tstar, m);
figure
plot(t_span, X(1, :), t_span, X(2,:))
xlabel('t')
legend('x(t)', 'y(t)')

X_rif = X(1, :);
M = m;

%% Controllo dell'errore

mrange = 2 .^ (4:8) + 1;
count = 0;
err = zeros (1, length (mrange));
for m = mrange
	count = count + 1;
	k = tstar / (m - 1);
	x = x0;
	X = zeros (2, m);
	X(:, 1) = x;
	for n = 1:m-1
		for ii = 1:s
			res = - JFun (x, K, k, ii) \ Fun (x, K, k, ii);
			it = 0;
			while ((it < maxit) && (norm (res) > tol))
				K(:, ii) = K(:, ii) + res;
				it = it + 1;
				res = - JFun (x, K, k, ii) \ Fun (x, K, k, ii);
			end
			K(:, ii) = K(:, ii) + res;
		end
		x = x + k * K * b.';
		X(:, n+1) = x;
	end
	err(count) = norm (X(1,:) - X_rif(1:((M - 1) / (m -1)):M), inf);
end

ord3 = err(1) * (mrange / mrange(1)) .^ (-3);
figure
loglog(mrange, ord3, mrange, err, 'o')
legend('ord3', 'err')
