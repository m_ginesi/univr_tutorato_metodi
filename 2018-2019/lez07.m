clear all
close all

%% Risolviamo il problema
%%
%% x_1' = x_1^2 + x_2^2 - x_1 x_2 + t^2
%% x_2' = t cos(x_1 x_2)
%%
%% col metodo Adams-Multon (multistep implicito)
%%
%%                    2
%% y_{n+1} = y_n + k sum b_i f(t_{n+i-1}, y_{n+i-1})
%%                   i=0
%%
%% dove i coefficienti b_i sono
%%
%% b_0 = -1/12,  b_1 = 2/3,  b_2 = 5/12

%% Dati
% Dati del problema
t0 = 0; % tempo iniziale
tstar = 3; % tempo finale
y0 = [1; 1]; % dato iniziale
% Funzione del problema ...
f = @(t, y) [y(1) - y(2) - y(1) * y(2) + t^2;
             t * cos(y(1) * y(2))];
% ... e relativo Jacobiano
Jf = @(t, y) [1 - y(2),                     - 1 - y(1);
              - t * y(2) * sin(y(1) * y(2)), - t * y(1) * sin(y(1) * y(2))];
% Dati del metodo
% Trapezi per il primo step
F_trapz = @(t, y, tn, yn, k) y - yn - k/2 * f(tn, yn) - k/2 * f(t, y);
JF_trapz = @(t, y, k) eye(2) - k/2 * Jf (t, y);
% Adams-Multon per gli altri
b = [-1/12, 2/3, 5/12];
F_am = @(t, y, tn, yn, tnn, ynn, k) y - yn - k * (b(1) * f(tnn, ynn) + ...
	b(2) * f(tn, yn) + b(3) * f(t, y));
JF_am = @(t, y, k) eye(2) - k * b(3) * Jf (t, y);
% Dati del metodo di Newton
maxit = 100;
tol = 1e-12;

%% Soluzione di riferimento
mrif = 2 ^ 10 + 1;
krif = (tstar - t0) / (mrif - 1);
Y = zeros (2, mrif);
Y(:, 1) = y0;
% Primo step con trapezi
yn = y0;
y = yn;
tn = t0;
t = tn + krif;
res = - JF_trapz (t, y, krif) \ F_trapz (t, y, tn, yn, krif);
it = 0;
while ((it < maxit) && (norm (res) > tol))
	it = it + 1;
	y = y + res;
	res = - JF_trapz (t, y, krif) \ F_trapz (t, y, tn, yn, krif);
end
y = y + res;
Y(:, 2) = y;
% Adams-Multon
tnn = t0;
tn = t0 + krif;
t = tn + krif;
ynn = y0;
yn = Y(:, 2);
for n = 2:mrif-1;
	y = yn; % guess iniziale
	res = - JF_am (t, y, krif) \ F_am (t, y, tn, yn, tnn, ynn, krif);
	it = 0;
	while ((it < maxit) && (norm(res) > tol))
		it = it + 1;
		y = y + res;
		res = - JF_am (t, y, krif) \ F_am (t, y, tn, yn, tnn, ynn, krif);
	end
	y = y + res;
	ynn = yn;
	tnn = tn;
	yn = y;
	tn = t;
	t = t + krif;
	Y(:, n+1) = y;
end

t_span = linspace (t0, tstar, mrif);
figure
plot(t_span, Y(1,:), t_span, Y(2,:))
legend('x_1', 'x_2')
xlabel('t')
title('Time evolution')

figure
plot(Y(1,:), Y(2,:))
xlabel('x_1')
ylabel('x_2')
title('Trajectory')

%% Controllo dell'ordine di convergenza
mrange = 2 .^ (3:9) + 1;
count = 0;
yrif = Y(:, mrif); % Calcoliamo l'errore usando la soluzione al tempo finale
for m = mrange
	count = count + 1;
	k = (tstar - t0) / (m - 1);
	% Primo step con trapezi
	yn = y0;
	y = yn; % guess iniziale
	tn = t0;
	t = tn + k;
	res = - JF_trapz (t, y, k) \ F_trapz (t, y, tn, yn, k);
	it = 0;
	while ((it < maxit) && (norm (res) > tol))
		it = it + 1;
		y = y + res;
		res = - JF_trapz (t, y, k) \ F_trapz (t, y, tn, yn, k);
	end
	y = y + res;
	Y(:, 2) = y;
	% Adams-Multon
	tnn = t0;
	tn = t0 + k;
	t = tn + k;
	ynn = y0;
	yn = y;
	for n = 2:m-1;
		y = yn; % guess iniziale
		res = - JF_am (t, y, k) \ F_am (t, y, tn, yn, tnn, ynn, k);
		it = 0;
		while ((it < maxit) && (norm(res) > tol))
			it = it + 1;
			y = y + res;
			res = - JF_am (t, y, k) \ F_am (t, y, tn, yn, tnn, ynn, k);
		end
		y = y + res;
		ynn = yn;
		tnn = tn;
		yn = y;
		tn = t;
		t = t + k;
	end
	err(count) = norm(y - yrif, inf);
end

ord3 = err(1) * (mrange / mrange(1)) .^ (-3);
figure
loglog(mrange, ord3, '-', mrange, err, 'x')
legend('Ordine 3', 'Errore')
