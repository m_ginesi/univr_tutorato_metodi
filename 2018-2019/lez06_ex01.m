clear all
close all

%% Risolviamo il seguente problema (detto moto armonico smorzato)
%%
%% x''(t) = K (g - x(t)) - D x'(t)
%% x(0) = x_0
%% x'(0) = x_0'
%%
%% la cui soluzione esatta si trova facilmente (hint. metodo degli autovalori)

%% Dati

tstar = 3; % tempo finale
K = 10; % coefficiente elastico
D = 2 * sqrt (K); % coefficiente di smorzamento
g = 0; % posizione di "goal"
A = [0, 1; -K, -D]; % matrice del problema
b = [0; K*g]; % termine affine

x0 = 2; % posizione iniziale
v0 = 0; % velocita` iniziale

mrange = 2 .^ (4:10) + 1;
count = 0;

err_ee = zeros (1, length (mrange));
err_ei = zeros (1, length (mrange));
err_trpz = zeros (1, length (mrange));

%% Soluzione esatta

Delta = D ^ 2 - 4 * K;
lambda1 = (- D + sqrt (Delta)) / 2;
lambda2 = (- D - sqrt (Delta)) / 2;

c = [1, 1; lambda1, lambda2] \ [x0-g; v0];

% Estraiamo la parte reale per evitare eventuali componenti immaginarie spurie
x_sol_f = @(t) real (c(1) * exp (lambda1 * t) + c(2) * exp (lambda2 * t) + g);

% Stampiamo a schermo che tipo di smorzamento abbiamo
tol = 1e-14; % tolleranza per Delta
if (Delta > tol)
	disp ("Il sistema e` sovrasmorzato")
elseif (Delta < - tol)
	disp ("Il sistema e` sottosmorzato")
else
	disp ("Il sistema e` smorzato criticamente")
end

%% Ciclo sul numero di passi di discretizzazione

for m = mrange
	count = count + 1;
	k = tstar / (m - 1);
	t = linspace (0, tstar, m);
	x_sol = x_sol_f (t);
	% Eulero Esplicito
	x = [x0; v0];
	x_ee = zeros (1, m);
	x_ee(1) = x(1);
	for n = 2:m
		x = x + k * A * x + k * b;
		x_ee(n) = x(1);
	end
	err_ee(count) = norm (x_ee - x_sol, inf);
	% Eulero Implicito
	x = [x0; v0];
	x_ei = zeros (1, m);
	x_ei(1) = x(1);
	B = eye (2) - k * A;
	[L, U] = lu (B);
	for n = 2:m
		x = U \ (L \ (x + k * b));
		x_ei(n) = x(1);
	end
	err_ei(count) = norm (x_ei - x_sol, inf);
	% Trapezi
	x = [x0; v0];
	x_trpz = zeros (1, m);
	x_trpz(1) = x(1);
	C = eye (2) - k * A / 2;
	[L, U] = lu (C);
	D = eye(2) + k * A / 2;
	for n = 2:m
		x = U \ (L \ (D * x));
		x_trpz(n) = x(1);
	end
	err_trpz(count) = norm (x_trpz - x_sol, inf);
end

%% Plot

err_norm = (err_ee(1) * err_ei(1) * err_trpz(1)) ^ (1/3);
ord1 = err_norm * (mrange / mrange(1)) .^ (-1);
ord2 = err_norm * (mrange / mrange(1)) .^ (-2);
figure
loglog (mrange, ord1, '-', mrange, ord2, '-', mrange, err_ee, 'x', ...
	mrange, err_ei, 'o', mrange, err_trpz, '*')
legend('ordine1', 'ordine2', 'Eulero Esplicito', 'Eulero Implicito', 'Trapezi')
