clear all
close all

%% Risolviamo il problema
%%
%%  u_t = u_xx + (1 - x^2)(1 + sqrt(t)),  x \in (0, 1)
%%  u_x(t, 0) = 0
%%  u(t, 1) = 0
%%  u(0, x) = 1 - x^2
%%
%% Utilizzando Eulero implicito nel tempo

%% Dati
m_x = 2 ^ (10) + 1;
x = linspace (0, 1, m_x).';
h = 1 / (m_x - 1);
D2 = toeplitz (sparse ([1, 1], [1, 2], [-2, 1]/h/h, 1, m_x));
D2(1,1:2) = [-2, 2] / h / h;
D2(m_x, m_x-1:m_x) = [0, 0];
b_fun = @(t) ([1 - x(1:m_x-1); 0] .^ 2) * (1 + sqrt(t));
t0 = 0;
tf = 1;
u0 = 1 - x .^ 2;

%% Soluzione di riferimento
m_t = 2 ^ 10 + 1;
k = (tf - t0) / (m_t - 1);
u = u0;
A = speye(m_x) - k * D2;
t = t0;
for n = 1:m_t - 1
	u = A \ (u + k * b_fun(t + k));
	t = t + k;
end

u_rif = u;

figure
plot(x, u);
title ('Soluzione di riferimento')

%% Controllo dell'errore
mrange = 2 .^ (4:8) + 1;
count = 0;
err = zeros (1, length (mrange));
for m_t = mrange
	count = count + 1;
	k = (tf - t0) / (m_t - 1);
	u = u0;
	A = speye(m_x) - k * D2;
	t = t0;
	for n = 1:m_t - 1
		u = A \ (u + k * b_fun(t + k));
		t = t + k;
	end
	err(count) = norm (u - u_rif, inf);
end

ord1 = err(1) * (mrange / mrange(1)) .^ (-1);

figure
loglog (mrange, ord1, mrange, err, 'x')
xlabel('m_t')
legend('ord1', 'effettivo')
