clear all
close all

%% Risolviamo il problema
%%
%% u u_{xx} - (u_x) ^ 2 = - 3 x ^ 4  x \in (0, 1)
%% u (0) = 0
%% u (1) = 1
%%
%% la cui soluzione esatta e`
%%
%% u(x) = x ^ 3

%% Dati
xl = 0; % Estremo sx del dominio
xr = 1; % Estremo dx del dominio
mrange = 2 .^ (4:10) + 1; % Insieme del numero di passi di discretizzazione
count = 0; % Counter
err = zeros (1, length (mrange)); % Inizializziamo il vettore degli errori
tol = 1e-10; % Tolleranza per il metodo di Newton
maxit = 50; % Numero massimo di iterazioni per il metodo di Newton

%% Ciclo sul numero di passi di discretizzazione
for m = mrange
	count = count + 1; % Incrementiamo il counter
	x = linspace (xl, xr, m).'; % Dominio discretizzato scritto come colonna
	h = (xr - xl) / (m - 1); % Passo di discretizzazione
	% Definiamo le matrici di discretizzazione delle derivate
	D1 = toeplitz (sparse (1, 2, -1/2/h, 1, m), sparse (1, 2, 1/2/h, 1, m));
	D2 = toeplitz (sparse ([1,1], [1,2], [-2,1]/h/h, 1, m));
	% Rimuoviamo la prima e ultima riga delle matrici di discretizzazione. Questo
	% per rendere piu` facile porre le condizioni al bordo. Creiamo anche una
	% matrice identita` senza prima e ultima riga
	D1 = D1(2:m-1, :);
	D2 = D2(2:m-1, :);
	Id = speye(m);
	Id = Id(2:m-1, :);
	% Definiamo la funzione da azzerare per risolvere il problema ed il suo
	% Jacobiano
	Fun = @(u) [u(1) - 0; ...
	                   (D2 * u) .* (Id * u) - (D1 * u) .^ 2 + 3 * Id * x .^ 4;...
	                                                                   u(m) - 1];
	% In riga i il problema discretizzato e`
	%        u_{i-1} - 2 u_i + u_{i+1}     (u_{i+1} - u_{i-1}) ^ 2
	% u_i * --------------------------- - -------------------------
	%                   h^2                        4 h^2
	Jfun = @(u) spdiags ([[-u(1:m-2) + 2 * u(2:m-1) + u(3:m); 0; 0]/2/h/h,...
	                          [1; (u(1:m-2) - 4 * u(2:m-1) + u(3:m))/h/h; 1], ...
	          [0; 0; u(1:m-2) + 2 * u(2:m-1) - u(3:m)]/2/h/h], [-1, 0, 1], m, m);
	%% Metodo di Newton
	u = linspace (0, 1, m).'; % Guess iniziale
	it = 0; % Resettiamo il counter del metodo
	res = -Jfun(u) \ Fun (u);
	while ((norm (res) >  tol) && (it <= maxit))
		u = u + res;
		it = it + 1;
		res = -Jfun(u) \ Fun (u);
	end
	u = u + res;
	usol = x .^ 3;
	err(count) = norm (u - usol, inf);
end

ord2 = err(1) * (mrange / mrange(1)) .^ (-2);
figure
loglog (mrange, ord2, '-', mrange, err, 'o')
xlabel ('m')
legend ('atteso', 'effettivo')
