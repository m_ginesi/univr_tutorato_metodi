clear all
close all

%% Risolviamo il problema
%%
%%  u_t = c u_x + d u_{xx} + cos (u)   x \in (0, 2\pi), t > 0
%%  u (0, x) = 1 - cos (u)
%%  u_x (t, 0) = 0
%%  u_x (t, 1) = 0
%%
%% Utilizzando il metodo dei trapezi

%% Dati
c = - 1;
d = 1 / 100;
xl = 0;
xr = 2 * pi;
t0 = 0;
tf = 1;

m_x = ceil ((xr - xl) / (d / abs (c))) + 1;
m_x = max (m_x , 100);
h = (xr - xl) / (m_x - 1);
x = linspace (xl, xr, m_x).';
u0 = 1 - cos (x);
D1 = toeplitz (sparse (1, 2, -1, 1, m_x), sparse (1, 2, 1, 1, m_x)) / 2 / h;
D2 = toeplitz (sparse ([1, 2], [1, 1], [-2, 1], m_x, 1)) / h / h;
D1(1, 1:2) = [0, 0];
D1(m_x, m_x-1:m_x) = [0, 0];
D2(1,1:2) = [-2, 2] / h / h;
D2(m_x, m_x-1:m_x) = [2, -2] / h / h;
A = c * D1 + d * D2;
b_fun = @(u) cos(u);
J_b = @(u) spdiags (-sin(u), 0, m_x, m_x);
Fun = @(u, un, k) u - (un + k/2 * (A * un + b_fun(un) + A * u + b_fun(u)));
JFun = @(u, k) speye (m_x) - k/2 * (A + J_b(u));
tol = 1e-10;
maxit = 100;

%% Soluzione di riferimento
m_t = 2 ^ 10 + 1;
k = (tf - t0) / (m_t - 1);
un = u0;
t = t0;
for n = 1:m_t - 1
	u = un; % guess iniziale
	res = - JFun (u, k) \ Fun (u, un, k);
	it = 0;
	while (it < maxit && norm (res) > tol)
		it = it + 1;
		u = u + res;
		res = - JFun (u, k) \ Fun (u, un, k);
	end
	u = u + res;
	un = u;
end
u_rif = u;

%% Controllo dell'errore
mrange = 2 .^ (4:8) + 1;
count = 0;
err = zeros (1, length (mrange));
for m_t = mrange
	count = count + 1;
	k = (tf - t0) / (m_t - 1);
	un = u0;
	for n = 1:m_t - 1
		u = un;
		it = 0;
		res = - JFun (u, k) \ Fun (u, un, k);
		while (it < maxit && norm (res) > tol)
			u = u + res;
			res = - JFun (u, k) \ Fun (u, un, k);
			it = it + 1;
		end
		u = u + res;
		un = u;
	end
	err(count) = norm (u - u_rif, inf);
end

ord2 = err(1) * (mrange / mrange(1)) .^ (-2);
figure
loglog (mrange, ord2, mrange, err, 'x')
