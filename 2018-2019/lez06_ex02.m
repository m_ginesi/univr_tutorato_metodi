clear all
close all

%% Risolviamo il seguente problema (detto moto armonico smorzato)
%%
%% x''(t) = K (g - x(t)) - D x'(t)
%% x(0) = x_0
%% x'(0) = x_0'
%%
%% la cui soluzione esatta si trova facilmente (hint. metodo degli autovalori)

%% Dati

tstar = 3; % tempo finale
K = 10; % coefficiente elastico
D = 2 * sqrt (K); % coefficiente di smorzamento
g = 0; % posizione di "goal"
A = [0, 1; -K, -D]; % matrice del problema
b = [0; K*g]; % termine affine

x0 = 2; % posizione iniziale
v0 = 0; % velocita` iniziale

mrange = 2 .^ (4:10) + 1;
count_m = 0;
% Insieme dei valori di theta
theta_range = [0, 1, 2, 3, 4, 5, 6] / 6;
count_theta = 0;

err = zeros (length (theta_range), length (mrange));

%% Soluzione esatta

Delta = D ^ 2 - 4 * K;
lambda1 = (- D + sqrt (Delta)) / 2;
lambda2 = (- D - sqrt (Delta)) / 2;

c = [1, 1; lambda1, lambda2] \ [x0-g; v0];

% Estraiamo la parte reale per evitare eventuali componenti immaginarie spurie
x_sol_f = @(t) real (c(1) * exp (lambda1 * t) + c(2) * exp (lambda2 * t) + g);

% Stampiamo a schermo che tipo di smorzamento abbiamo
tol = 1e-14; % tolleranza per Delta
if (Delta > tol)
	disp ("Il sistema e` sovrasmorzato")
elseif (Delta < - tol)
	disp ("Il sistema e` sottosmorzato")
else
	disp ("Il sistema e` smorzato criticamente")
end

%% Ciclo sul numero di passi di discretizzazione

for theta = theta_range
	count_theta = count_theta + 1;
	count_m = 0;
	for m = mrange
		count_m = count_m + 1;
		k = tstar / (m - 1);
		t = linspace (0, tstar, m);
		x_sol = x_sol_f (t);
		B = eye(2) - k * theta * A;
		[L, U] = lu(B);
		C = eye(2) + k * (1 - theta) * A;
		Y = zeros(2, m);
		Y(:, 1) = [x0; v0];
		for n = 1:m-1
			Y(:, n + 1) = U \ (L \ (C * Y(:, n) + k * b));
		end
		err(count_theta, count_m) = norm (Y(1,:) - x_sol);
	end
end

%% Plot
sym = {'x', 'o', '<', '>', '^', 'v', 's'};
err_norm = (prod (err(:,1))) ^ (1/7);
ord1 = err_norm * (mrange / mrange(1)) .^ (-1);
ord2 = err_norm * (mrange / mrange(1)) .^ (-2);
figure
loglog (mrange, ord1, '-', mrange, ord2, '-')
hold on
for it = 1:count_theta
	loglog (mrange, err(it, :), sym{it})
end
hold off
