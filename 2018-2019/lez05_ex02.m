clear all
close all

%% Risolviamo il problema
%%  y'''' = 5 y'' - 4 y
%% la cui soluzione generica si trova facilmente col metodo degli autovalori:
%%  y = c1 exp(-t) + c2 exp(t) + c3 exp(-2t) + c4 exp(2t)
%% Chiaramente, per risolvere il problema, bisogna porlo in forma normale:
%%  z = [y; y'; y''; y''']
%% Calcoleremo l'errore in due modi:
%%  - l'errore dell'intera evoluzione della sola y
%%  - l'errore fatto al solo tempo finale per y, y', y'', y'''

%% Dati
z0 = [0; 1; -1; 0];
% Si puo` utilizzare qualsiasi dato iniziale
z0 = rand(4, 1);
tstar = 1;
mrange = 2 .^ (4:10) + 1;
count = 0;
err_sol = zeros (1, length (mrange));
err_t_f = zeros (1, length (mrange));
% f(z) = A * z
A = [ 0, 1, 0, 0;
      0, 0, 1, 0;
      0, 0, 0, 1;
     -4, 0, 5, 0];
% Soluzione al tempo finale per tutte le componenti
zsol_tf = expm(A * tstar) * z0;
% Evoluzione temporale di y lungo il tempo.
% Sistema per trovare i coefficienti
C_in = [ 1, 1,  1, 1;
        -1, 1, -2, 2;
         1, 1,  4, 4;
        -1, 1, -8, 8];
c = C_in \ z0;
% Soluzione
ysol = @(t) c(1) * exp(-t) + c(2) * exp(t) + c(3) * exp(-2 * t) + ...
	c(4) * exp (2 * t);

%% Ciclo sul numero di passi di discretizzazione
for m = mrange
	count = count + 1;
	k = tstar / (m - 1);
	t = linspace (0, tstar, m);
	% Il metodo e` y+ = y + k * A * y = (I + k * A) * y
	B = eye(4) + k * A;
	% La soluzione sara` una matrice Z = [z_{ij}] = [y^(i) (t_j)]
	Z = zeros(4, m);
	Z(:, 1) = z0;
	for it = 1:m-1
		Z(:, it+1) = B * Z(:, it);
	end
	err_sol(count) = norm (Z(1, :) - ysol(t), inf);
	err_t_f(count) = norm (Z(:, m) - zsol_tf, inf);
end

% Scrivo l'ordine in modo tale che nel grafico appaia in mezzo ai due
% errori effettivi
ord1 = sqrt (err_sol(1) * err_t_f(1)) * (mrange / mrange(1)) .^ (-1);
figure
loglog (mrange, ord1, '-', mrange, err_sol, 'x', mrange, err_t_f, 'o')
legend ('ordine 1', 'y', 'z_f')
