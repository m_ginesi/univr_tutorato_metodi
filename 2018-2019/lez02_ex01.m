clear all
close all

%% Risolviamo il problema
%%
%% u_{xx} + 2 u_x + u = 2 exp (x)  x \in (0, 1)
%% u_x (0) = 1
%% u (1) = (e - e^{-1}) / 2
%%
%% la cui soluzione esatta e`
%%
%% u(x) = sinh (x)

%% Dati
xl = 0; % Estremo sx del dominio
xr = 1; % Estremo dx del dominio
mrange = 2 .^ (4:10) + 1; % Insieme del numero di passi di discretizzazione
count = 0; % Counter
err = zeros (1, length (mrange)); % Inizializziamo il vettore degli errori

%% Ciclo sul numero di passi di discretizzazione
for m = mrange
	count = count + 1; % Incrementiamo il counter
	x = linspace (xl, xr, m).'; % Dominio discretizzato scritto come colonna
	h = (xr - xl) / (m - 1); % Passo di discretizzazione
	% Definiamo le matrici di discretizzazione delle derivate
	D1 = toeplitz (sparse (1, 2, -1/2/h, 1, m), sparse (1, 2, 1/2/h, 1, m));
	D2 = toeplitz (sparse ([1,1], [1,2], [-2,1]/h/h, 1, m));
	% Definiamo la matrice del problema
	A = D2 + 2*D1 + speye (m);
	% Definiamo il termine noto
	b = 2 * exp (x);
	% Poniamo le condizioni al contorno
	A(1, 1:2) = [1 - 2/h/h, 2/h/h];
	b(1) = 2/h;
	A(m, m-1:m) = [0, 1];
	b(m) = (exp (1) - exp (-1)) / 2;
	% Risolviamo il problema
	u = A \ b;
	usol = sinh (x);
	err(count) = norm (u - usol, inf);
end

ord2 = err(1) * (mrange / mrange(1)) .^ (-2);
figure
loglog (mrange, ord2, '-', mrange, err, 'o')
xlabel ('m')
legend ('atteso', 'effettivo')
