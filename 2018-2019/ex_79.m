clear all
close all

%% Esercizio 79 temi d'esame

%% Dati
% Dati del problema
A_tilde = [-500, 0; 0, -1];
b_fun = @(t) [500*cos(t) - sin(t); sin(t) + cos(t)];
y0 = [1; 0];
t0 = 0;
tf = 2 * pi;
% Dati del metodo
theta = 2 - sqrt(2);
A = [0, 0, 0;
     theta/2, theta/2, 0;
     (3*theta - theta^2 - 1)/2/theta, (1 - theta)/2/theta, theta/2];
b = [(3*theta - theta^2 - 1)/2/theta, (1 - theta)/2/theta, theta/2];
c = sum (A, 1);

%% Soluzione di riferimento
m = 2 ^ 10 + 1;
y = y0;
Y = zeros (2, m);
Y(:, 1) = y;
t = t0;
k = (tf - t0) / (m - 1);
K = zeros (2, 3);
for n = 1:m-1
	K(:, 1) = A_tilde * y + b_fun(t);
	K(:, 2) = (eye(2) - k * A(2,2) * A_tilde) \ ...
		(A_tilde * y + k * A(2,1) * A_tilde * K(:, 1) + b_fun(t + k * c(2)));
	K(:, 3) = (eye(2) - k * A(3,3) * A_tilde) \ ...
		(b_fun(t + k * c(3)) + A_tilde * (y + k * A(3,1) * K(:,1) + k * A(3,2) * K(:,2)));
	y = y + k * K * b.';
	Y(:, n+1) = y;
end
y_rif = Y(2, :);
m_rif = m;

t_span = linspace (t0, tf, m);
figure
plot (t_span, Y(1,:), t_span, Y(2,:))
legend ('y_1','y_2')
xlabel('t')

%% Controllo dell'errore
mrange = 2 .^ (4:8) + 1;
count = 0;
err = zeros (1, length (mrange));
for m = mrange
	count = count + 1;
	y = y0;
	Y = zeros (2, m);
	Y(:, 1) = y;
	t = t0;
	k = (tf - t0) / (m - 1);
	K = zeros (2, 3);
	for n = 1:m-1
		K(:, 1) = A_tilde * y + b_fun(t);
		K(:, 2) = (eye(2) - k * A(2,2) * A_tilde) \ ...
			(A_tilde * y + k * A(2,1) * A_tilde * K(:, 1) + b_fun(t + k * c(2)));
		K(:, 3) = (eye(2) - k * A(3,3) * A_tilde) \ ...
			(b_fun(t + k * c(3)) + A_tilde * (y + k * A(3,1) * K(:,1) + k * A(3,2) * K(:,2)));
		y = y + k * K * b.';
		Y(:, n+1) = y;
	end
	err(count) = norm (Y(2,:) - y_rif(1:(m_rif - 1)/(m - 1):m_rif), inf);
end

ord1 = err(1) * (mrange / mrange(1)) .^ (-1);
ord2 = err(1) * (mrange / mrange(1)) .^ (-2);
ord3 = err(1) * (mrange / mrange(1)) .^ (-3);
ord4 = err(1) * (mrange / mrange(1)) .^ (-4);

figure
loglog (mrange, ord1, mrange, ord2, mrange, ord3, mrange, ord4, mrange, err, 'x')
legend ('ord1', 'ord2', 'ord3', 'ord4', 'effettivo')
