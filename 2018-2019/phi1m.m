function [N,PHI0] = phi1m(A)
%
% function N = phi1m(A)
%

% Scale A by power of 2 so that its norm is < 1/2 .
[f,e] = log2(norm(A,1));    % f.*2.^e=norm(A,1)
s = min(max(0,e+1),1023);
A = A/2^s;  % una potenza di 2 e' rappresentabile in aritmetica binaria, quindi non produce errore di arrotondamento(se non fossero potenze di 2 introdurremmo degli errori)
% Pade approximation for phi1(z)
ID = eye(size(A));
n = [1,1/30,1/30,1/936,1/4680,1/171600,1/3603600,1/259459200];  % coeff del numeratorore dell'appr. di pade
d = [1,-7/15,1/10,-1/78,1/936,-1/17160,1/514800,-1/32432400];   % coeff del denominatore dell'appr. di pade
q = length(n);
N = n(1)*ID;    % valore iniziale del denominatore
D = d(1)*ID;
X = ID;
for i = 2:q
   X = A*X;
   N = N+n(i)*X;
   D = D+d(i)*X;
end
N = full(D\N);  % A puo' essere una matrice sparsa
% Undo scaling by repeated squaring
PHI0 = A*N+ID;  % sara' l'exp
for i = 1:s
  N = (PHI0+ID)*N/2;
  PHI0 = PHI0*PHI0;     % esponenziale nel doppio
end


% gli appunti di questa parte sono a pag. 25
