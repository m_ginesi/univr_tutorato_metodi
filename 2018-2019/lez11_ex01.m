clear all
close all

%% Risolviamo il problema
%%  y''   = 2 y + 4 t^2 exp (t^2)
%%  y(0)  = 1
%%  y'(0) = 0
%% la cui soluzione esatta e`
%%  y(t) = exp (t ^ 2)
%% utilizzando i metodi Eulero esponenziale ed Eulero Punto Medio

%% Dati
% Dati del problema
t0 = 0;
tf = 1;
A = [0, 1; 2, 0]; % parte lineare del problema
b = @(t) [0; 4*t^2*exp(t^2)];
z0 = [1; 0];
z_sol = @(t) [exp(t^2); 2*t*exp(t^2)];
% Dati del metodo
mrange = 2 .^ (4:10);
count = 0;
err_ee = zeros (1, length (mrange));
err_pm = err_ee;

% Calcolo dell'errore
for m = mrange
	count = count + 1;
	k = (tf - t0) / (m - 1);
	z_ee = z0; % inizializziamo la soluzione calcolata con Eulero esponenziale
	z_pm = z0; % inizializziamo la soluzione calcolata con Esponenziale punto medio
	t = 0; % tempo
	P = phi1m (k * A);
	for n = 1:m-1
		z_ee = z_ee + k * P * (A * z_ee + b(t));
		z_pm = z_pm + k * P * (A * z_pm + b(t + k / 2));
		t = t + k;
	end
	err_ee(count) = norm(z_ee - z_sol(t), inf);
	err_pm(count) = norm(z_pm - z_sol(t), inf);
end

ord1 = err_ee(1) * (mrange / mrange(1)) .^ (-1);
ord2 = err_pm(1) * (mrange / mrange(1)) .^ (-2);

figure
loglog (mrange, ord1, mrange, ord2, mrange, err_ee, 'x', mrange, err_pm, 'o')
legend('ord1', 'ord2', 'Eulero esponenziale', 'Esponenziale punto medio')
