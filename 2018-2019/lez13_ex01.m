clear all
close all

%% Risolviamo il problema
%%
%%  u_t = u_{xx} + x (1 - x) u   x \in (0, 1), t > 0
%%  u (0, x) = x (1 - x)
%%  u (t, 0) = t ^ 2
%%  u_x (t, 1) = t - 1
%%
%% utilizzando il metodo Eulero Esponenziale

%% Dati
xl = 0;
xr = 1;
t0 = 0;
tf = 1;
m_x = 2 ^ 8 + 1;
x = linspace (xl, xr, m_x).';
h = (xr - xl) / (m_x - 1);
u0 = x .* (1 - x);
A = toeplitz (sparse ([1, 2], [1, 1], [-2, 1] / h / h, m_x, 1));
A(1, 1:2) = [0, 0];
A(m_x, m_x-1:m_x) = [2, -2] / h / h;
b_fun = @(t, u) [0; x(2:m_x) .* (1 - x(2:m_x)) .* u(2:m_x)] + ...
	sparse ([1, m_x], [1, 1], [2 * t, 2 * (t - 1) / h], m_x, 1); % corregge le condizioni al bordo

%% Soluzione di riferimento
m_t = 2 ^ 10 + 1;
k = (tf - t0) / (m_t - 1);
P = phi1m (k * A);
u = u0;
% Salviamo l'evoluzione temporale delle condizioni al bordo
u_dir = zeros (1, m_t);
u_neum = zeros (1, m_t);
u_dir(1) = u(1);
%u_neum(1) = (u(m_x) - u(m_x - 1)) / h; % stima della derivata di ordine 1
u_neum(1) = (0.5 * u(m_x - 2) - 2 * u(m_x - 1) + 1.5 * u(m_x)) / h; % stima della derivata di ordine 2
t = t0;
for n = 1:m_t - 1
	% Risolviamo
	u = u + k * P * (A * u + b_fun (t, u));
	% Aggiorniamo l'evoluzione delle condizioni al bordo
	u_dir(n + 1) = u(1);
	u_neum(n + 1) = (0.5 * u(m_x - 2) - 2 * u(m_x - 1) + 1.5 * u(m_x)) / h;
	% Aggiorniamo l'evoluzione temporale
	t = t + k;
end
u_rif = u;

% Plot della soluzione di riferimento
figure
plot (x, u)
title ('u (t_f)')
xlabel ('x')

% Plot delle condizioni al bordo
t_span = linspace (t0, tf, m_t);
dir_ex = t_span .^ 2;
neum_ex = t_span - 1;

figure
subplot (211)
plot (t_span, u_dir, t_span, dir_ex, '--')
title ('Condizione di Dirichlet al bordo sinistro')
xlabel ('t')
legend ('Stimata', 'Esatta', 'location', 'southeast')
subplot (212)
plot (t_span, u_neum, t_span, neum_ex, '--')
title ('Condizione di Neuman al bordo sinistro')
xlabel ('t')
legend ('Stimata', 'Esatta', 'location', 'southeast')

figure
subplot (211)
plot (t_span, abs (u_dir - dir_ex))
title ('Errore per la condizione di Dirichlet al bordo sinistro')
xlabel ('t')
subplot (212)
plot (t_span, abs (u_neum - neum_ex))
title ('Errore per la condizione di Neuman al bordo destro')
xlabel ('t')

%% Controlliamo la convergenza del metodo
mrange =  2 .^ (4:8) + 1;
count = 0;
err = zeros (1, length (mrange));
for m_t = mrange
	count = count + 1;
	k = (tf - t0) / (m_t - 1);
	P = phi1m (k * A);
	u = u0;
	t = t0;
	for n = 1:m_t - 1
		u = u + k * P * (A * u + b_fun (t, u));
		t = t + k;
	end
	err(count) = norm (u - u_rif, inf);
end

ord1 = err(1) * (mrange / mrange(1)) .^ (-1);

figure
loglog (mrange, ord1, mrange, err, 'x')
legend ('Ordine 1', 'Effettivo')
