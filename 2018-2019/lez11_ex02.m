clear all
close all

%% Risolviamo il problema
%%  y_1' = cos(t) sin(y_2) cos(y_3)
%%  y_2' = sin(t) cos(y_1) sin(y_3)
%%  y_3' = cos(t) sin(y_1) cos(y_2)
%% utilizzando il metodo Eulero - Rosenbrock esponenziale.
%% Per fare cio`, il problema andra` scritto in forma autonoma!

%% Dati
t0 = 0;
tf = 3;
y0 = [1; 1; 1];
% Problema in forma normale ...
fun = @(y) [sin(y(2)) * cos(y(3)) * cos(y(4));
            cos(y(1)) * sin(y(3)) * sin(y(4));
            sin(y(1)) * cos(y(2)) * cos(y(4));
            1];
% ... e relativo Jacobiano
Jfun = @(y) [0,                                   cos(y(2)) * cos(y(3)) * cos(y(4)), -sin(y(2)) * sin(y(3)) * cos(y(4)), -sin(y(2)) * cos(y(3)) * sin(y(4));
             -sin(y(1)) * sin(y(3)) * sin(y(4)), 0,                                   cos(y(1)) * cos(y(3)) * sin(y(4)),  cos(y(1)) * sin(y(3)) * cos(y(4));
             cos(y(1)) * cos(y(2)) * cos(y(4)),  -sin(y(1)) * sin(y(2)) * cos(y(4)),  0,                                 -sin(y(1)) * cos(y(2)) * sin(y(4));
             0,                                  0,                                   0,                                  0];

%% Soluzione di riferimento
m = 2 ^ 12 + 1;
k = (tf - t0) / (m - 1);
y = [y0; t0]; % lo stato deve contenere anche il tempo
Y = zeros (4, m);
Y(:, 1) = y;
for n = 1:m-1
	y = y + k * phi1m(k * Jfun(y)) * fun(y);
	Y(:, n + 1) = y;
end
y_rif = y;
t_span = linspace (t0, tf, m);
figure
plot(t_span, Y(1, :), t_span, Y(2, :), t_span, Y(3, :), t_span, Y(4, :))
legend('y_1', 'y_2', 'y_3', 'y_4 = t')

%% Calcolo dell'errore
mrange = 2 .^ (4:10) + 1;
count = 0;
err = zeros (1, length(mrange));
for m = mrange
	count = count + 1;
	k = (tf - t0) / (m - 1);
	y = [y0; t0];
	for n = 1:m-1
		y = y + k * phi1m(k * Jfun(y)) * fun(y);
	end
	err(count) = norm (y - y_rif, inf);
end

ord2 = err(1) * (mrange / mrange(1)) .^ (-2);

figure
loglog (mrange, ord2, mrange, err, 'x')
legend ('ord2', 'effettivo')
