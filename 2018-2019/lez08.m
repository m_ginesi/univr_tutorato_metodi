clear all
close all

%% Risolviamo il problema
%%
%%  x_1 ' = 1 - 4 x_1 + x_1^2 x_2
%%  x_2' = 3 x_1 - x_1^2 x_2
%%
%% utilizzando il metodo "Punto medio implicito"

%% Dati
% Dati del problema
t0 = 0;
tstar = 3;
x0 = [0; 0];
fun = @(x) [1 - 4 * x(1) + x(1)^2 * x(2);
            3 * x(1) - x(1)^2 * x(2)];
Jfun = @(x) [-4 + 2 * x(1) * x(2), x(1)^2;
             3 - 2 * x(1) * x(2), -x(1)^2];
% Dati del metodo
Fun = @(x, xn, k) x - xn - k * fun((x + xn) / 2);
JFun = @(x, xn, k) eye(2) - k * Jfun((x + xn) / 2) / 2;
maxit = 100;
tol = 1e-10;

%% Soluzione di riferimento
m = 2 ^ 12 + 1;
k = (tstar - t0) / (m - 1);
X = zeros(2, m);
X(:, 1) = x0;
for n = 1:m-1
	xn = X(:, n);
	x = xn; % guess iniziale
	it = 0;
	% Metodo di Newton
	res = -JFun(x, xn, k) \ Fun(x, xn, k);
	while ((it < maxit) && (norm(res) >  tol))
		it = it + 1;
		x = x + res;
		res = -JFun(x, xn, k) \ Fun(x, xn, k);
	end
	x = x + res;
	X(:, n+1) = x;
end

x_rif = x;

%% Plottiamo la soluzione di riferimento
t_span = linspace(t0, tstar, m);

figure
plot (t_span, X(1,:), t_span, X(2,:))
xlabel('t')
legend('x_1', 'x_2')
title('Evoluzione temporale di x_1 ed x_2')

figure
hold on
plot (X(1,:), X(2,:), 'r')

x1 = linspace(0, 0.4, 20);
x2 = linspace(0, 2, 20);
[x1, x2] = meshgrid(x1, x2);
f1 = 1 - 4 * x1 + x1.^2 .* x2;
f2 = 3 * x1 - x1 .^ 2 .* x2;
quiver(x1, x2, f1, f2, 0.4)

xlabel('x_1')
ylabel('x_2')
title('Traiettoria')

%% Convergenza del metodo
mrange = 2 .^ (4:10) + 1;
err = zeros(1, length(mrange));
count = 0;
for m = mrange
	count = count + 1;
	k = (tstar - t0) / (m - 1);
	xn = x0;
	for n = 1:m-1
		x = xn; % guess iniziale
		it = 0;
		res = -JFun(x, xn, k) \ Fun(x, xn, k);
		while ((it < maxit) && (norm(res) > tol))
			x = x + res;
			it = it + 1;
			res = -JFun(x, xn, k) \ Fun(x, xn, k);
		end
		x = x + res;
		xn = x;
	end
	err(count) = norm (x - x_rif, inf);
end

ord1 = err(1) * (mrange / mrange(1)) .^ (-1);
ord2 = err(1) * (mrange / mrange(1)) .^ (-2);

figure
loglog(mrange, ord1, mrange, ord2, mrange, err, 'ok')
xlabel('m')
legend('ordine 1', 'ordine 2', 'effettivo')
title('Convergenza del metodo')

%% Ordine del metodo di Newton
k = 2 ^ (-8);
xn = rand(2,1) * 20;
x = rand(2,1) * 20;
err_n = [];
res = -JFun(x, xn, k) \ Fun(x, xn, k);
err_n(1) = norm(res);
it = 0;
while ((it < maxit) && (norm(res) > tol));
	it = it + 1;
	x = x + res;
	res = -JFun(x, xn, k) \ Fun(x, xn, k);
	err_n (it + 1) = norm (res);
end
figure
semilogy (0:it, err_n)
title('Convergenza per il Metodo di Newton')
xlabel('it')
