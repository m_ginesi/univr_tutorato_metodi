clear all
close all

%% Risolviamo il problema
%%  y'' - y = - exp (cos (t)) * cos(t) * (1 + cos(t))
%%  y(0) = exp (1)
%%  y'(0) = 0
%% la cui soluzione esatta e`
%%  y(t) = exp (cos (t))
%% col metodo RK semi-implicito (di ordine 2) di tableau
%%  1/4 | 1/4
%%  3/4 | 1/2  1/4
%%  ----+----------
%%      | 1/2  1/2
%% Si noti che il problema e` lineare, quindi non ci sara` bisogno di
%% usare il metodo di Newton

%% Dati
% Dati del problema
t0 = 0;
t_star = 1;
A_tilde = [0, 1; 1, 0]; % parte lineare del problema
b_tilde = @(t) [0; -exp(cos(t)) * cos(t) * (1 + cos(t))]; % componente affine del problema
z0 = [exp(1); 0];
z_sol = @(t) [exp(cos(t)); -exp(cos(t)).*sin(t)];
% Dati del metodo
A = [1, 0; 2, 1] / 4;
b = [1, 1] / 2;
c = sum (A, 2);
s = length (c); % numero di stadi
mrange = 2 .^ (4:10) + 1;
count = 0;
K = zeros(2, s); % matrice dei coefficienti di incremento
err = zeros (1, length (mrange));

%% Risoluzione del problema
for m = mrange
	count = count + 1;
	k = (t_star - t0) / (m - 1);
	z = z0;
	t = 0;
	for n = 1:m-1
		% Calcoliamo i coefficienti
		for idx = 1:s
			K(:, idx) = (eye(2) - k * A_tilde * A(idx, idx)) \ ...
				(A_tilde * (z + k * K(:, 1:idx-1) * A(idx, 1:idx-1).') + ...
				b_tilde(t + k * c(idx)));
		end
		t = t + k; % aggiorniamo il tempo
		z = z + k * K * b.'; % aggiorniamo la soluzione
	end
	err(count) = norm (z - z_sol(t), inf);
end

%% Controllo dell'errore

ord2 = err(1) * (mrange / mrange(1)) .^ (-2);
figure
loglog (mrange, ord2, mrange, err, 'x')
legend('ordine 2','effettivo')
