clear all
close all

%% Risolviamo il problema
%%
%%  x_1' = - x_1 ^ 3 - x_1 x_2 ^ 2 + x_1 + x_2
%%  x_2' = - x_2 ^ 3 - x_1 ^ 2 x_1 - x_1 + x_2
%%
%% con un metodo RK di tableau
%%
%%   0  |
%%  1/2 | 1/2
%%  1/2 |  0  1/2
%%   1  |  0   0   1
%%  ----+-----------------
%%      | 1/6 1/3 1/3 1/6

%% Dati
% Dati del problema
t0 = 0;
tf = 1;
fun = @(x) [- x(1)^3 - x(1) * x(2)^2 + x(1) + x(2);
            - x(2)^3 - x(1)^2 * x(1) - x(1) + x(2)];
x0 = [2; 2];

%% Soluzione di riferimento
mrif = 2 ^ 12 + 1;
k = (tf - t0) / (mrif - 1);
X = zeros (2, mrif);
X(:, 1) = x0;
for n = 1:mrif-1
	K1 = fun (X(:, n));
	K2 = fun (X(:, n) + k * K1 / 2);
	K3 = fun (X(:, n) + k * K2 / 2);
	K4 = fun (X(:, n) + k * K3);
	X(:, n+1) = X(:, n) + k * (K1 + 2 * K2 + 2 * K3 + K4) / 6;
end

x_rif = X(:, n+1);

[X1, X2] = meshgrid (linspace(-2, 2, 50), linspace(-2, 2, 50));
U = - X1 .^ 3 - X1 .* X2 .^ 2 + X1 + X2;
V = - X2 .^ 3 - X1 .^ 2 .* X1 - X1 + X2;
% Scaliamo il campo vettoriale per il plot
U = U ./ sqrt(U .^ 2 + V .^ 2);
V = V ./ sqrt(U .^ 2 + V .^ 2);

figure
hold on
quiver(X1, X2, U, V)
plot(X(1, :), X(2, :), '-r', 'LineWidth', 2)
axis([-2, 2, -2, 2])
xlabel('x_1')
ylabel('x_2')

t_span = linspace(t0, tf, mrif);
figure
plot(t_span, X(1, :), t_span, X(2, :))
xlabel('t')
legend('x_1', 'x_2')

%% Controllo dell'ordine del metodo
mrange = 2 .^ (5:10) + 1;
err = zeros (1, length (mrange));
count = 0;
for m = mrange
	count = count + 1;
	k = (tf - t0) / (m - 1);
	x = x0;
	for n = 1:m-1
		K1 = fun (x);
		K2 = fun (x + k * K1 / 2);
		K3 = fun (x + k * K2 / 2);
		K4 = fun (x + k * K3);
		x = x + k * (K1 + 2 * K2 + 2 * K3 + K4) / 6;
	end
	err(count) = norm (x - x_rif, inf);
end

ord1 = err(1) * (mrange / mrange(1)) .^ (-1);
ord2 = err(1) * (mrange / mrange(1)) .^ (-2);
ord3 = err(1) * (mrange / mrange(1)) .^ (-3);
ord4 = err(1) * (mrange / mrange(1)) .^ (-4);
ord5 = err(1) * (mrange / mrange(1)) .^ (-5);
figure
loglog (mrange, ord1, mrange, ord2, mrange, ord3, mrange, ord4, mrange, ord5, mrange, err, 'xk')
xlabel('m')
legend('ord1', 'ord2', 'ord3', 'ord4', 'ord5', 'err')