clear all
close all

%% Risolviamo l'ODE
%%  y' = y - cos(t)
%% con Eulero Esplicito, scrivendolo sia in forma non autonoma (quella
%% presentata) che in forma autonoma.
%% La soluzione generica e`
%%  C exp (t) - sin(t)/2 + cos(t)/2

%% Dati
t0 = 0;
y0 = 0;
tstar = 1;
mrange = 2 .^ (4:10) + 1;
% Inizializziamo i vettori di errore ...
err_n_a = zeros(1, length(mrange));
err_a = zeros(1, length(mrange));
% ... e di tempo computazionale
time_n_a = zeros(1, length(mrange));
time_a = zeros(1, length(mrange));
% Soluzione esatta
c = y0 - 1/2;
ysol = @(t) c * exp (t) - sin (t) / 2 + cos (t) / 2;

%% Forma non autonoma
f = @(t, y) y - cos (t);
count = 0;
for m = mrange
	count = count + 1;
	k = tstar / (m - 1);
	t = linspace (0, tstar, m);
	y = zeros (1, m);
	y(1) = y0;
	tic
	for it = 1:m-1
		y(it + 1) = y(it) + k * f (t(it), y(it));
	end
	time_n_a(count) = toc;
	err_n_a(count) = norm (y - ysol(t), inf);
end

ord1 = err_n_a(1) * (mrange / mrange(1)) .^ (-1);

figure
subplot (211)
loglog (mrange, ord1, '-', mrange, err_n_a, 'x')
title ('Problema in forma non autonoma')
subplot (212)
loglog (mrange, time_n_a)

%% Forma autonoma
g = @(z) [z(1) - cos(z(2)); 1];
count = 0;
for m = mrange
	count = count + 1;
	k = tstar / (m - 1);
	t = linspace (0, tstar, m);
	z = zeros (2, m);
	z(:, 1) = [y0; t0];
	tic
	for it = 1:m-1
		z(:, it + 1) = z(:, it) + k * g (z(:, it));
	end
	time_n_a(count) = toc;
	err_n_a(count) = norm (z(1,:) - ysol(t), inf);
end

ord1 = err_n_a(1) * (mrange / mrange(1)) .^ (-1);

figure
subplot (211)
loglog (mrange, ord1, '-', mrange, err_n_a, 'x')
title ('Problema in forma autonoma')
subplot (212)
loglog (mrange, time_n_a)
