clear all
close all

%% Risolviamo il problema lineare
%%
%% 2 u_x - x u_{xx} = 0
%% u(0) = 0
%% u(1) = 1
%%
%% la cui soluzione esatta e`
%%
%% x ^ 3

%% Dati
xl = 0; % estremo sinistro del dominio
xr = 1; % estremo destro del dominio
u_a = 0; % condizione al bordo sinistro
u_b = 1; % condizione al bordo destro
mrange = 2 .^ (4:10) + 1; % insieme del numero di passi di discretizzazione
count = 0; % inizializziamo il counter
err = zeros (1, length (mrange)); % inizializziamo il vettore degli errori

%% Ciclo sul numero di passi di discretizzazione
for m = mrange
	count = count + 1; % incrementiamo il counter
	x = linspace (xl, xr, m).'; % dominio
	h = (xr - xl) / (m - 1); % passo di discretizzazione
	% Definiamo la matrici di discretizzazione della derivata prima
	D1 = toeplitz (sparse (1, 2, -1/2/h, 1, m), sparse (1, 2, 1/2/h, 1, m));
	% Scriviamo la matrice della derivata seconda in modo che tenga gia` conto
	% della moltiplicazione per x
	diag_sup = [0; x(1:m-1)]/h/h;
	diag_cen = -2*x/h/h;
	diag_inf = [x(2:m);0]/h/h;
	xD2 = spdiags ([diag_inf, diag_cen, diag_sup], [-1,0,1], m, m);
	%--------------------------------------------------------------------------
	%% /!\ In Matlab si potrebbe usare il broadcasting: il comnando
	% xD2 = D2 .* x;
	%% e` equivalente a
	% xD2 = D2 .* repmat(x, 1, m);
	%% In Octave non funziona con matrici sparse, ma solo con quelle piene.
	%% In ogni caso, e` preferibile evitare il broadcasting, per essere sempre
	%% sicuri di cosa si stia facendo.
	%%--------------------------------------------------------------------------
	% Scriviamo la matrice del problema
	A = 2 * D1 - xD2;
	% ed il termine noto
	b = zeros (m, 1);
	% Poniamo le condizioni al contorno
	A(1, 1:2) = [1, 0];
	A(m, m-1:m) = [0, 1];
	b(1) = u_a;
	b(m) = u_b;
	% Risolviamo il problema
	u = A \ b;
	% Calcoliamo l'errore
	u_sol = x .^ 3;
	err(count) = norm (u - u_sol, inf);
end

%% Plot
ord = err(1) * (mrange / mrange(1)) .^ (-2); % andamento dell'errore atteso
figure
loglog (mrange, ord, '-', mrange, err, 'x')
legend ('errore atteso', 'errore effettivo')
xlabel ('m')
