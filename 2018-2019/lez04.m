clear all
close all

%% Esercizio 81

%% COMMENTO RISPETTO A LEZIONE
%% Ho dimenticato un segno - nel creare la matrice B, in particolare nella
%% diagonale inferiore (vedere commenti con /!\)

%% Dati
maxit = 100;
tol = 1e-12;
mrange = 2 .^ (4:10) + 1;
count = 0;
err = zeros(1, length(mrange));

%% Soluzione di riferimento
M = 2 ^ 12 + 1;
X = linspace (0, 2, M).';
H = 2 / (M - 1);
% Scirviamo la matrice che implementa (1 + x) * ddx
diag_s_A = [0; X(1:M-1) + 1] / H / H;
diag_p_A = -2 * (X + 1) / H / H;
diag_i_A = [X(2:M) + 1; 0] / H / H;
A = spdiags([diag_s_A, diag_p_A, diag_i_A], [1, 0, -1], M, M);
% Scirviamo la matrice che implementa x^2 * dx
diag_s_B = [0; X(1:M-1).^2] / 2 / H;
% /!\ Errore! Ho sbagliato un segno
% diag_i_B = [X(2:M).^2; 0] / 2 / H;
diag_i_B = [-X(2:M).^2; 0] / 2 / H;
B = spdiags([diag_s_B, diag_i_B], [1, -1], M, M);
% Parte lineare del problema
C = -A + B;
% Parte lineare delle condizioni al contorno
C(1,1:2) = [1, 0];
C(M,M-1:M) = [-6, 6] / H / H;
% Termine noto
b = sparse (1, 1, 1, M, 1);
% Parte non lineare del problema ...
fun = @(u) [0; 1./(1+u(2:M).^2)];
% ... e relativo jacobiano
jfun = @(u) spdiags ([0; -2*(1+u(2:M).^2).^(-2).*u(2:M)], 0, M, M);
% Scriviamo la funzione da azzerare per risolvere il problema ...
Fun = @(u) C * u - fun(u) - b;
% ... ed il suo jacobiano
JFun = @(u) C - jfun(u);
% Metodo di Newton
U = ones (M, 1);
res = -JFun(U) \ Fun(U);
it = 0;
while ((norm(res, inf) > tol) && (it < maxit))
	U = U + res;
	it = it + 1;
	res = -JFun(U) \ Fun(U);
end
U = U + res;
figure
plot(X, U)
title("Soluzione di riferimento")

for m = mrange
	x = linspace (0, 2, m).';
	h = 2 / (m - 1);
	count = count + 1;
	% Scirviamo la matrice che implementa (1 + x) * ddx
	diag_s_A = [0; x(1:m-1) + 1] / h / h;
	diag_p_A = -2 * (x + 1) / h / h;
	diag_i_A = [x(2:m) + 1; 0] / h / h;
	A = spdiags([diag_s_A, diag_p_A, diag_i_A], [1, 0, -1], m, m);
	% Scirviamo la matrice che implementa x^2 * dx
	diag_s_B = [0; x(1:m-1).^2] / 2 / h;
	% /!\ Errore! ho dimenticato un segno -
	% diag_i_B = [x(2:m).^2; 0] / 2 / h;
	diag_i_B = [-x(2:m).^2; 0] / 2 / h;
	B = spdiags([diag_s_B, diag_i_B], [1, -1], m, m);
	% Parte lineare del problema
	C = -A + B;
	% Parte lineare delle condizioni al contorno
	C(1,1:2) = [1, 0];
	C(m,m-1:m) = [-6, 6] / h / h;
	% Termine noto
	b = sparse (1, 1, 1, m, 1);
	% Parte non lineare del problema ...
	fun = @(u) [0; 1./(1+u(2:m).^2)];
	% ... e relativo jacobiano
	jfun = @(u) spdiags ([0; -2*(1+u(2:m).^2).^(-2).*u(2:m)], 0, m, m);
	% Scriviamo la funzione da azzerare per risolvere il problema ...
	Fun = @(u) C * u - fun(u) - b;
	% ... ed il suo jacobiano
	JFun = @(u) C - jfun(u);
	% Metodo di Newton
	u = ones (m, 1);
	res = -JFun(u) \ Fun(u);
	it = 0;
	while ((norm(res, inf) > tol) && (it < maxit))
		u = u + res;
		it = it + 1;
		res = -JFun(u) \ Fun(u);
	end
	u = u + res;
	urif = U(1:(M-1)/(m-1):M);
	err(count) = norm(u - urif, inf);
end

ord2 = err(1) * (mrange / mrange(1)) .^ (-2);
figure
loglog (mrange, ord2, '-', mrange, err, 'x')
