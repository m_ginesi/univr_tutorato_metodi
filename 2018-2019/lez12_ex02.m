clear all
close all

%% Risolviamo il problema
%%
%%  u_t = u_xx + x exp(t) cos(u),  x \in (0, pi/2)
%%  u(t, 0) = 1
%%  u_x(t, pi/2) = -1
%%  u(0, x) = 1 - x^2
%%
%% Utilizzando Eulero esponenziale nel tempo

%% Dati
m_x = 2 ^ 8 + 1;
x = linspace (0, pi / 2, m_x).';
h = pi / 2 / (m_x - 1);
D2 = toeplitz (sparse ([1, 1], [1, 2], [-2, 1]/h/h, 1, m_x));
D2(1,1:2) = [0, 0];
D2(m_x, m_x-1:m_x) = [2, -2] / h / h;
b_fun = @(t, u) [0; x(2:m_x) * exp(t) .* cos(u(2:m_x))] + sparse (m_x, 1, -2 / h, m_x, 1);
t0 = 0;
tf = 1;
u0 = cos (x);

%% Soluzione di riferimento
m_t = 2 ^ 9 + 1;
k = (tf - t0) / (m_t - 1);
u = u0;
t = t0;
P = phi1m (k * D2);
for n = 1:m_t - 1
	u = u + k * P * (D2 * u + b_fun(t, u));
	t = t + k;
end

u_rif = u;

figure
plot(x, u)
axis([0, pi/2, 0.5, 1.2])
xlabel('x')
title ('Soluzione al tempo finale')

%% Controllo dell'errore

mrange = floor (2 .^ linspace(3, 7, 10) + 1);
err = zeros (1, length (mrange));
count = 0;
for m_t = mrange
	count = count + 1;
	k = (tf - t0) / (m_t - 1);
	u = u0;
	t = t0;
	P = phi1m (k * D2);
	for n = 1:m_t - 1
		u = u + k * P * (D2 * u + b_fun(t, u));
		t = t + k;
	end
	err(count) = norm (u - u_rif, inf);
end

ord1 = err(1) * (mrange / mrange(1)) .^ (-1);

figure
loglog (mrange, ord1, mrange, err, 'x')
xlabel ('m')
legend ('ord1', 'effettivo')
