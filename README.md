# README #

Repository contenente delle note (incomplete) sulla parte di tutorato del corso
"Metodi Numerici per Equazioni Differenziali" dell'Università degli Studi di
Verona. Il corso è tenuto dal prof. [Marco Caliari](http://profs.scienze.univr.it/~caliari/)

### Per ottenere gli appunti

andare nella cartella notes
```
cd /home/io/blabla/tutorato_metodi/notes/
```
e lanciare il comando make
```
make
```
La prima compilazione puo` richiedere del tempo (devono essere
generate tutte le figure)
