graphics_toolkit("gnuplot")
mrange = 2.^(4:10);
count = 0;
for m = mrange+1
count = count + 1;
t = linspace(1,2,m);
y_ex = t.^4;
k = 1/(m-1);
y_E = zeros(3,m);
y_E(:,1) = [1;4;12];
f = @(y) [y(2);y(3);96*y(1)/y(2)];
for n = 1:m-1
  y_E(:,n+1) = y_E(:,n) + k*f(y_E(:,n));
end
err_E(count)=norm(y_ex - y_E(1,:),inf);
F = @(yo,yn) yn - k/2*f(yn) - yo - k/2*f(yo);
JF = @(yn) eye(3) -k/2*[0,1,0;0,0,1;96/yn(2),-96*yn(1)/(yn(2)^2),0];
y_T = zeros(3,m);
y_T(:,1) = [1;4;12];
maxit = 30;
tol = 1e-10;
for n = 1:m-1
  y_T(:,n+1) = y_T(:,n);
  res = -JF(y_T(:,n+1))\F(y_T(:,n),y_T(:,n+1));
  it = 0;
  while it<maxit && norm(res)>tol
    y_T(:,n+1) = y_T(:,n+1) + res;
    res = -JF(y_T(:,n+1))\F(y_T(:,n),y_T(:,n+1));
    it = it+1;
  end
end
err_T(count) = norm(y_T(1,:)-y_ex,inf);
end
ord1 = err_E(1)*(mrange/mrange(1)).^(-1);
ord2 = err_T(1)*(mrange/mrange(1)).^(-2);
figure(1)
subplot(1,2,1)
loglog(mrange,err_E,'o',mrange,ord1,'-k')
legend('err','ord1')
title('Eulero esplicito')
subplot(1,2,2)
loglog(mrange,err_T,'o',mrange,ord2,'-k')
legend('err','ord2')
title('Trapezi')
print('-depslatexstandalone','NL_eul_plot','-F:14')
system('latex NL_eul_plot.tex');
system('dvips -E NL_eul_plot.dvi -o NL_eul_plot.eps');

