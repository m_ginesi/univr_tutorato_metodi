graphics_toolkit("gnuplot")
clear all
close all
clc
mrange = 2.^(4:10);
count = 0;
N_it = 0;
N_it_in = 0;
for m = mrange
  count = count + 1;
  x = linspace(1,2,m);
  x = x';
  h = 1/(m-1);
  D1 = toeplitz(sparse(1,2,-1/(2*h),1,m),sparse(1,2,1/(2*h),1,m));
  D2 = toeplitz(sparse([1,1],[1,2],[-2,1]/(h^2),1,m));
  b = zeros(m,1);
  b(1) = 1;
  b(m) = 1/(4*h);
  F = @(u) [u(1);(u.*(D1*u)+0.5*D2*u)(2:m-1);u(m)*(-1/4-1/h^2) + ...
    u(m-1)/h^2] - b;
  D2(1,1:2) = [1,0]*2;
  D2(m,m-1:m) = [1/h^2,-1/4-1/h^2]*2;
  Jfun = @(u) 0.5*D2 + spdiags([[-u(2:m-1);0;0],[0;u(3:m)-u(1:m-2);0],...
    [0;0;u(2:m-1)]],[-1,0,1],m,m)/(2*h);
  u_ex = x.^(-1);
  tol = 1e-12;
  maxit = 50;
  it = 0;
  u = ones(m,1);
  res = -Jfun(u)\F(u);
  while ((norm(res,inf)>tol) && (it<maxit))
    u = u+res;
    res = -Jfun(u)\F(u);
    it = it+1;
  end
  N_it = N_it + it;
  err(count) = norm(u-u_ex,inf);
end
ord2 = err(1)*(mrange/mrange(1)).^(-2);
figure(1)
loglog(mrange,err,'o',mrange,ord2,'-k')
legend('errore','ordine2')
xlabel('m')
ylabel('err')
print('-depslatexstandalone','FD_NL_2_plot','-F:14')
system('latex FD_NL_2_plot.tex');
system('dvips -E multistep_plot.dvi -o FD_NL_2_plot.eps');

