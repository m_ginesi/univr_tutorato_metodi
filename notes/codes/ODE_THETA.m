graphics_toolkit("gnuplot")
mrange = 2.^(4:10);
count = 0;
for m = mrange+1
count = count+1;
t = linspace(0,1,m);
y_ex = sin(t).^2;
k = 1/(m-1);
y = zeros(3,m);
y(:,1) = [0;0;2];
A = [0,1,0;0,0,1;-2,-4,-1];
#thetarange = linspace(0,1,9);
#thetarange = asin(linspace(-1,1,9))/pi + 1/2;
thetarange = (linspace(-1,1,9).^3)/2 + 1/2;
iter = 0;
for theta = thetarange
  iter = iter+1;
  for n = 1:m-1
    b = k*theta*[0;0;2*cos(t(n+1))^2] + (1-theta)*k*[0;0;2*cos(t(n))^2] + (eye(3)+k*(1-theta)*A)*y(:,n);
    B = eye(3)-k*theta*A;
    y(:,n+1) = B\b;
  end
  err(iter,count) = norm(y(1,:)-y_ex,inf);
end
end
ord1 = 1e-2*(mrange/mrange(1)).^(-1);
ord2 = 1e-2*(mrange/mrange(1)).^(-2);
figure(1)
loglog(mrange,err(1,:),'o',mrange,err(2,:),'x',mrange,err(3,:),'s',mrange,err(4,:),'^',mrange,err(5,:),'v',mrange,err(6,:),'<',mrange,err(7,:),'>',mrange,err(8,:),'p',mrange,err(9,:),'h',mrange,ord1,'--k',mrange,ord2,':k')
legend(num2str(thetarange(1)),num2str(thetarange(2)),num2str(thetarange(3)),num2str(thetarange(4)),num2str(thetarange(5)),num2str(thetarange(6)),num2str(thetarange(7)),num2str(thetarange(8)),num2str(thetarange(9)),'ord1','ord2')
xlabel('m')
ylabel('err')
print('-depslatexstandalone','Theta_plot','-F:14')
system('latex Theta_plot.tex');
system('dvips -E Theta_plot.dvi -o Theta_plot.eps');

