graphics_toolkit("gnuplot")
clear all
close all
## Esplicito
A = sparse([2,3,4],[1,2,3],[0.5,0.5,1],4,4);
b = [1,2,2,1]/6;
c = [0,1,1,2]/2;
y0 = [0;0;2];
AA = [0,1,0;0,0,1;-2,-4,-1];
f = @(t,y) AA*y + [0;0;2*cos(t)^2];
#f = @(t,y) [y(2);y(3);96*y(1)/y(2)];
#y0 = [1;4;12];
mrange = 2.^(4:10);
count = 0;
for m = mrange + 1
  count++;
  h = pi/(m-1);
  #h = 1/(m-1);
  Y = NaN(3,m);
  Y(:,1) = y0;
  T = linspace(pi,2*pi,m);
  #T = linspace(1,2,m);
  for n = 1:m-1
  K = zeros(3,4);
  K(:,1) = feval(f,T(n),Y(:,n));
  K(:,2) = feval(f,T(n) + h*c(2),Y(:,n) + h*A(2,1)*K(:,1));
  K(:,3) = feval(f,T(n) + h*c(3),Y(:,n) + ...
    h*(A(3,1)*K(:,1)+A(3,2)*K(:,2)));
  K(:,4) = feval(f,T(n) + h*c(4),Y(:,n) + ...
    h*(A(4,1)*K(:,1)+A(4,2)*K(:,2)+A(4,3)*K(:,3)));
  Y(:,n+1) = Y(:,n) + h*K*b';
  endfor
  ysol = sin(T).^2;
  #ysol = T.^4;
  err(count) = norm(Y(1,:)-ysol,inf);
endfor
ord4 = err(1)*(mrange/mrange(1)).^(-4);
figure(1)
subplot(1,2,1)
loglog(mrange,err,'o',mrange,ord4,'-k')
title('RK esplicito')
legend('err','ord4')
xlabel('m')
ylabel('err')
## RK semiimplicito
A = [3+sqrt(3),0;-2*sqrt(3),3+sqrt(3)]/6;
b = [1,1]/2;
c = [3+sqrt(3),3-sqrt(3)]/6;
y0 = [0;0;2];
AA = [0,1,0;0,0,1;-2,-4,-1];
f = @(t,y) AA*y + [0;0;2*cos(t)^2];
#f = @(t,y) [y(2);y(3);96*y(1)/y(2)];
#y0 = [1;4;12];
mrange = 2.^(4:10);
count = 0;
for m = mrange + 1
  count++;
  h = pi/(m-1);
  #h = 1/(m-1);
  Y = NaN(3,m);
  Y(:,1) = y0;
  T = linspace(pi,2*pi,m);
  #T = linspace(1,2,m);
  for n = 1:m-1
  K = zeros(3,2);
  K(:,1) = (eye(3)-h*A(1,1)*AA)\(AA*Y(:,n)+[0;0;2*cos(T(n)+h*c(1))^2]);
  K(:,2) = (eye(3)-h*A(2,2)*AA)\(AA*(Y(:,n)+h*A(2,1)*K(:,1))+...
    [0;0;2*cos(T(n)+c(2)*h)^2]);
  Y(:,n+1) = Y(:,n) + h*K*b';
  endfor
  ysol = sin(T).^2;
  #ysol = T.^4;
  err(count) = norm(Y(1,:)-ysol,inf);
endfor
ord3 = err(1)*(mrange/mrange(1)).^(-3);
figure(1)
subplot(1,2,2)
loglog(mrange,err,'o',mrange,ord3,'-k')
legend('err','ord3')
title('RK semi-implicito')
xlabel('m')
ylabel('err.')
print('-depslatexstandalone','RK_plot','-F:14')
system('latex RK_plot.tex');
system('dvips -E RK_plot.dvi -o RK_plot.eps');

