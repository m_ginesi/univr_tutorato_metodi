graphics_toolkit("gnuplot")
clear all
close all
clc
mrange = 2.^(4:10);
count = 0;
for m = mrange
  count = count + 1;
  x = linspace(0,1,m);
  x = x';
  h = 1/(m-1);
  D1 = toeplitz(sparse(1,2,-1/(2*h),1,m),sparse(1,2,1/(2*h),1,m));
  D2 = toeplitz(sparse([1,1],[1,2],[-2,1]/(h^2),1,m));
  A1 = D2 + D1 + speye(m);
  A2 = A1;
  A3 = A1;
  A1(1,1:2) = [1,0];
  A1(m,m-1:m) = [0,1];
  b1 = x.^3+x-4;
  b2 = b1;
  b3 = b1;
  b1(1) = 1;
  b1(m) = 0;
  A2(1,1:2) = [1-2/h^2,2/h^2];
  A2(m,m-1:m) = [2/h^2,1-2/h^2];
  b2(1) = -5+2/h;
  b2(m) = 4/h;
  A3(1,1:2) = [2-2/h-2/h^2,2/h^2];
  A3(m,m-1:m) = [0,1];
  b3(1) = -4;
  b3(m) = 0;
  u1 = A1\b1;
  u2 = A2\b2;
  u3 = A3\b3;
  u_ex = x.^3-3*x.^2+x+1;
  err1(count) = norm(u1-u_ex,inf);
  err3(count) = norm(u3-u_ex,inf);
  err2(count) = norm(u2-u_ex,inf);
end

ord2 = mrange.^(-2);
figure(1)
loglog(mrange,err1,'s',mrange,err2,'o',mrange,err3,'x',mrange,ord2,'-k')
legend('Dirichlet','Neumann','Robin','ordine 2')
xlabel('m')
ylabel('err')
print('-depslatexstandalone','FD_multi_plot','-F:14')
system('latex FD_multi_plot.tex')
system('dvips -E FD_multi_plot.dvi -o FD_multi_plot.eps');

