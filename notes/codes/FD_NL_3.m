graphics_toolkit("gnuplot")
clear all
close all
clc
M = 2^14+1;
x = linspace(0,1,M);
x = x';
H = 1/(M-1);
D1 = toeplitz(sparse(1,2,-1/(2*H),1,M),sparse(1,2,1/(2*H),1,M));
D2 = toeplitz(sparse([1,1],[1,2],[-2,1]/(H^2),1,M));
b = linspace(0,1,M);
b = b';
b(1) = 0;
b(M) = 1;
F = @(u) [u(1);(D2*u+sin(D1*u)+u)(2:M-1);u(M)] - b;
D2(1,1:2) = [0,0];
D2(M,M-1:M) = [0,0];
Jfun = @(u) D2 + speye(M) + spdiags([[-cos((u(3:M)-u(1:M-2))/(2*H));0;0],[0;0;cos((u(3:M)-u(1:M-2))/(2*H))]],[-1,1],M,M)/(2*H);
tol = 1e-12;
maxit = 50;
it = 0;
U = ones(M,1);
res = -Jfun(U)\F(U);
while ((norm(res,inf)>tol) && (it<maxit))
  U = U+res;
  res = -Jfun(U)\F(U);
  it = it+1;
end
mrange = 2.^(4:10)+1;
count = 0;
N_it = 0;
N_it_in = 0;
for m = mrange
  count = count + 1;
  x = linspace(0,1,m);
  x = x';
  h = 1/(m-1);
  D1 = toeplitz(sparse(1,2,-1/(2*h),1,m),sparse(1,2,1/(2*h),1,m));
  D2 = toeplitz(sparse([1,1],[1,2],[-2,1]/(h^2),1,m));
  b = linspace(0,1,m);
  b = b';
  b(1) = 0;
  b(m) = 1;
  F = @(u) [u(1);(D2*u+sin(D1*u)+u)(2:m-1);u(m)] - b;
  D2(1,1:2) = [0,0];
  D2(m,m-1:m) = [0,0];
  Jfun = @(u) D2 + speye(m) + spdiags([[-cos((u(3:m)-u(1:m-2))/(2*h));0;0],...
    [0;0;cos((u(3:m)-u(1:m-2))/(2*h))]],[-1,1],m,m)/(2*h);
  tol = 1e-12;
  maxit = 50;
  it = 0;
  u = ones(m,1);
  res = -Jfun(u)\F(u);
  while ((norm(res,inf)>tol) && (it<maxit))
    u = u+res;
    res = -Jfun(u)\F(u);
    it = it+1;
  end
  N_it = N_it + it;
  err(count) = norm(u-U(1:(M-1)/(m-1):M),inf);
end
ord2 = err(1)*(mrange/mrange(1)).^(-2);
figure(1)
loglog(mrange,err,'o',mrange,ord2,'-k')
legend('errore','ordine2')
xlabel('m')
ylabel('err')
print('-depslatexstandalone','FD_NL_3_plot','-F:14')
system('latex FD_NL_3_plot.tex');
system('dvips -E FD_NL_3_plot.dvi -o FD_NL_3_plot.eps');

