graphics_toolkit("gnuplot")
mrange = 2.^(4:10);
count = 0;
for m = mrange+1
  count = count +1;
  t1 = linspace(2,3,m);
  k1 = 1/(m-1);
  y1 = zeros(1,m);
  y1(1) = 1;
  for n = 1:m-1
    y1(n+1) = y1(n) + k1*(5*y1(n)-3);
  end
  y_ex1 = 2/5*exp(5*(t1-2)) + 3/5;
  err1(count) = norm(y1-y_ex1,inf);
  figure(1)
  subplot(2,2,1)
  plot(t1,y1,t1,y_ex1)
  t2 = linspace(pi,2*pi,m);
  k2 = pi/(m-1);
  f2 = @(y) [-3*y(1)+sin(2*y(2));1];
  y2 = zeros(2,m);
  y2(:,1) = [0;pi];
  for n = 1:m-1
    y2(:,n+1) = y2(:,n) + k2*feval(f2,(y2(:,n)));
  end
  y_ex2 = (3*sin(2*t2)-2*cos(2*t2)+2*exp(3*pi-3*t2))/13;
  err2(count) = norm(y2(1,:)-y_ex2,inf);
  figure(1)
  subplot(2,2,2)
  plot(t2,y2(1,:),t2,y_ex2)
  t3 = linspace(0,1,m);
  k3 = 1/(m-1);
  f3 = @(y) [y(2);y(3);-y(3)-4*y(2)-2*y(1)+2*cos(y(4))^2;1];
  y3 = zeros(4,m);
  y3(:,1) = [0;0;2;0];
  for n = 1:m-1
    y3(:,n+1) = y3(:,n) + k3*feval(f3,(y3(:,n)));
  end
  y_ex3 = sin(t3).^2;
  err3(count) = norm(y3(1,:)-y_ex3,inf);
  figure(1)
  subplot(2,2,3)
  plot(t3,y3(1,:),t3,y_ex3)
  t4 = linspace(0,1,m);
  k4 = 1/(m-1);
  y4 = zeros(3,m);
  y4(:,1) = [7;-7;11];
  A = k4*[0,1,0;0,0,1;-1,-3,-3];
  for n=1:m-1
    y4(:,n+1) = y4(:,n) + A*y4(:,n);
  end
  y4_ex = 7*exp(-t4) + 2*t4.^2.*exp(-t4);
  err4(count) = norm(y4(1,:)-y4_ex,inf);
  figure(1)
  subplot(2,2,4)
  plot(t4,y4(1,:),t4,y4_ex)
  pause(0.5)
end
ord1 = (mrange/mrange(1)).^(-1);
close all
figure(1)
loglog(mrange,err1,'o',mrange,err4,'s',mrange,err2,'x',mrange,err3,'^',mrange,ord1,'-k')
legend('pbm 1','pbm2','pbm3','pbm4','ord1')
xlabel('m')
ylabel('err')
print('-depslatexstandalone','Eul_esp_plot','-F:14')
system('latex Eul_esp_plot.tex');
system('dvips -E Eul_esp_plot.dvi -o Eul_esp_plot.eps');

