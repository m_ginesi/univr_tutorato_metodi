graphics_toolkit("gnuplot")
mrange = 2.^(4:10);
count = 0;
tol = 1e-10;
maxit = 30;
for m = mrange
  count = count +1;
  t1 = linspace(2,3,m);
  k1 = 1/(m-1);
  y1 = zeros(1,m);
  y1(1) = 1;
  for n = 1:m-1
    y1(n+1) = (y1(n)*(1+5*k1/2)-3*k1)/(1-5*k1/2);
  end
  y_ex1 = 2/5*exp(5*(t1-2)) + 3/5;
  err1(count) = norm(y1-y_ex1,inf);
  t2 = linspace(pi,2*pi,m);
  k2 = pi/(m-1);
  y2 = zeros(1,m);
  y2(1) = 0;
  for n = 1:m-1
    y2(n+1) = -(k2/2*(-sin(2*t2(n+1))+3*y2(:,n)-sin(2*t2(n))) - y2(:,n))/(1+3*k2/2);
  end
  y_ex2 = (3*sin(2*t2)-2*cos(2*t2)+2*exp(3*pi-3*t2))/13;
  err2(count) = norm(y2(1,:)-y_ex2,inf);
  t3 = linspace(0,1,m);
  k3 = 1/(m-1);
  y3 = zeros(3,m);
  y3(:,1) = [0;0;2];
  A = [0,1,0;0,0,1;-2,-4,-1];
  for n = 1:m-1
    b = -k3*[0;0;cos(t3(n+1))^2] - y3(:,n)-k3*[0;0;cos(t3(n))^2] - k3/2*A*y3(:,n);
    B = -(eye(3)-k3/2*A);
    y3(:,n+1) = B\b;
  end
  y_ex3 = sin(t3).^2;
  err3(count) = norm(y3(1,:)-y_ex3,inf);
  t4 = linspace(0,1,m);
  k4 = 1/(m-1);
  y4 = zeros(3,m);
  y4(:,1) = [7;-7;11];
  A = k4/2*[0,1,0;0,0,1;-1,-3,-3];
  for n=1:m-1
    y4(:,n+1) = (eye(3)-A)\((eye(3)+A)*y4(:,n));
  end
  y4_ex = 7*exp(-t4) + 2*t4.^2.*exp(-t4);
  err4(count) = norm(y4(1,:)-y4_ex,inf);
end
ord1 = (mrange/mrange(1)).^(-2);
figure(1)
loglog(mrange,err1,'o',mrange,err4,'s',mrange,err2,'^',mrange,err3,'v',mrange,ord1,'-k')
legend('pbm 1','pbm2','pbm3','pbm4','ord2')
xlabel('m')
ylabel('err')
print('-depslatexstandalone','Trap_plot','-F:14')
system('latex Trap_plot.tex');
system('dvips -E Trap_plot.dvi -o Trap_plot.eps');

