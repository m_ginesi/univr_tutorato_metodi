graphics_toolkit("gnuplot")
clear all
close all

A = [0,1,0;0,0,1;-2,-4,-1];
f = @(t,y) A*y + [0;0;2*cos(t)^2];
mrange = 2.^(4:10);
count = 0;
b0 = 5/12;
b1 = -4/3;
b2 = 23/12;
for m = mrange+1
  T = linspace(0,1,m);
  k = 1/(m-1);
  count++;
  Y = NaN(3,m);
  Y(:,1) = [0,0,2];
  #Y(:,2) = Y(:,1) + k*f(T(1),Y(:,1));
  Y(:,2) = (eye(3) - k/2*A)\((k/2*A+eye(3))*Y(:,1) + k*[0;0;cos(T(1))^2 + cos(T(2))^2]);
  Y(:,3) = Y(:,2) + k *(-0.5*f(T(1),Y(:,1)) + 1.5*f(T(2),Y(:,2)));
  for n = 3:m-1
    Y(:,n+1) = Y(:,n) + k*(b0*f(T(n-2),Y(:,n-2)) + b1*f(T(n-1),Y(:,n-1)) + b2*f(T(n),Y(:,n)));
  endfor
  ysol = sin(T).^2;
  err(count) = norm(Y(1,:)-ysol,inf);
endfor 
ord3 = err(1)*(mrange/mrange(1)).^(-3);
figure(1)
subplot(1,2,1)
loglog(mrange,err,'o',mrange,ord3,'-k')
title('Adams-Bashforth')
legend('err','ord3')
xlabel('m')
%%%%%%%%%%%
mrange = 2.^(4:10);
count = 0;
maxit = 50;
tol = 1e-10;
f = @(y) [y(2);y(3);96*y(1)/y(2)];
j = @(y) [0,1,0;0,0,1;96/y(2),-96*y(1)/(y(2)^2),0];
for m = mrange+1;
  count++;
  T = linspace(1,2,m);
  ysol = T.^4;
  k = 1/(m-1);
  % Trapezi per il primo passo
  Ftrap = @(yn,yo) yn - 0.5*k*f(yn) - yo - 0.5*k*f(yo);
  Jtrap = @(yn) eye(3) - 0.5*k*j(yn);
  % Forma generale del metodo multistep
  b0 = -1/12;
  b1 = 2/3;
  b2 = 5/12;
  FF =  @(yn2,yn1,yn) yn2-(yn1+k*(b0*f(yn)+b1*f(yn1)+b2*f(yn2)));
  JJ = @(yn2) eye(3) -k*b2*j(yn2);
  Y = NaN(3,m);
  Y(:,1) = [1;4;12];
  yn = ones(3,1);
  yo = Y(:,1);
  res = -Jtrap(yn)\Ftrap(yn,yo);
  it = 0;
  while it<maxit && norm(res)>tol
    yn = yn + res;
    res = -Jtrap(yn)\Ftrap(yn,yo);
    it++;
  endwhile
  yn = yn + res;
  Y(:,2) = yn;
  % Inizio Multon
  for n = 1:m-2
    yn = Y(:,n);
    yn1 = Y(:,n+1);
    yn2 = Y(:,n+1); %initial guess
    res = -JJ(yn2)\FF(yn2,yn1,yn);
    it = 0;
      while it<maxit && norm(res)>tol
        yn2 = yn2 + res;
        res = -JJ(yn2)\FF(yn2,yn1,yn);
        it++;
      endwhile
    Y(:,n+2) = yn2;
  endfor
  err(count) = norm(Y(1,:)-ysol,inf);
endfor
ord3 = err(1)*(mrange/mrange(1)).^(-3);
figure(1)
subplot(1,2,2)
loglog(mrange,err,'o',mrange,ord3,'-k')
title('Adams-Multon')
legend('err','ord3')
xlabel('m')
print('-depslatexstandalone','multistep_plot','-F:14')
system('latex multistep_plot.tex');
system('dvips -E multistep_plot.dvi -o multistep_plot.eps');

