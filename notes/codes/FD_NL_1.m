graphics_toolkit("gnuplot")
clear all
close all
clc
mrange = 2.^(4:10);
count = 0;
N_it = 0;
N_it_in = 0;
for m = mrange
  count = count + 1;
  x = linspace(1,3,m);
  x = x';
  h = 2/(m-1);
  D1 = toeplitz(sparse(1,2,-1/(2*h),1,m),sparse(1,2,1/(2*h),1,m));
  D2 = toeplitz(sparse([1,1],[1,2],[-2,1]/(h^2),1,m));
  b = 4+x.^3/4;
  b(1) = 17;
  b(m) = 43/3;
  F = @(u) [u(1);(D2*u+1/8*((D1*u).*u))(2:m-1);u(m)] - b;
  D2(1,1:2) = [1,0];
  D2(m,m-1:m) = [0,1];
  Jfun = @(u) D2 + spdiags([[-u(2:m-1);0;0],[0;u(3:m)-u(1:m-2);0],...
    [0;0;u(2:m-1)]],[-1,0,1],m,m)/(16*h);
  u_ex = x.^2+16./x;
  tol = 1e-12;
  maxit = 50;
  it = 0;
  u = linspace(17,43/3,m);
  u = u';
  % Newton esatto
  res = -Jfun(u)\F(u);
  while ((norm(res,inf)>tol) && (it<maxit))
    u = u+res;
    res = -Jfun(u)\F(u);
    it = it+1;
  end
  N_it = N_it + it;
  err(count) = norm(u-u_ex,inf);
  it = 0;
  u = linspace(17,43/3,m);
  u = u';
  % Newton inesatto
  JF = Jfun(u);
  res = -JF\F(u);
  while ((norm(res,inf)>tol) && (it<maxit))
    u = u+res;
    res = -JF\F(u);
    it = it+1;
  end
  N_it_in = N_it_in + it;
  err_in(count) = norm(u-u_ex,inf);
end
disp(sprintf('Numero medio di iterazioni per il metodo di Newton esatto: %d',N_it/length(mrange)))
disp(sprintf('Numero medio di iterazioni per il metodo di Newton esatto: %d',N_it_in/length(mrange)))
ord2 = err(1)*(mrange/mrange(1)).^(-2);
figure(1)
loglog(mrange,err,'o',mrange,err_in,'x',mrange,ord2,'-k')
legend('Newton esatto','Newton inesatto','ord2')
xlabel('m')
ylabel('err')
print('-depslatexstandalone','FD_NL_1_plot','-F:14')
system('latex FD_NL_1_plot.tex');
system('dvips -E FD_NL_1_plot.dvi -o FD_NL_1_plot.eps');
