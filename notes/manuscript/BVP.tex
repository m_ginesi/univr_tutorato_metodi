In questa prima parte considereremo i problemi con condizioni al contorno (in inglese, Boudary Values Problem o BVP).
Si tratta di equazioni differenziali al secondo ordine in un intervallo $(a,b)$ munite di \emph{condizioni al contorno}, ovvero di valori, determinati dal problema, imposti alla soluzione (o alla sua derivata prima).

\section{Caso lineare}

Inizieremo considerando tre problemi lineari, con la stessa soluzione e stessa equazione differenziale, ma considereremo diversi tipi di condizioni al contorno.
Per tutti i tre seguenti problemi, la soluzione \`e
\[ \hat{u}(x) = x^3-3x^2+x+1, \]
e l'equazione differenziale che considereremo è
\[ u + u' + u'' = x^3+x-4,\quad x\in (0,1). \]
D'ora in avanti, denoteremo con $\D_1$ e $\D_2$ le matrici che discretizzano, rispettivamente, la derivata prima e seconda, ovvero
\[
\D_1 = \frac{1}{2h}
\begin{bmatrix}
0      &  1     &  0     &  0     & \cdots &   0\\
-1     & 0      &  1     &  0     & \cdots &   0\\
0      & \ddots & \ddots & \ddots & \ddots & \vdots\\
\vdots & \ddots & \ddots & \ddots & \ddots & 0\\
0      & \cdots &  0     & -1     & 0      & 1\\
0      & \cdots &  0     & 0      &-1      &0
\end{bmatrix},
\]
ed
\[
\D_2 = \frac{1}{h^2}
\begin{bmatrix}
-2     &   1    &   0    & 0      & \cdots &   0\\
1      & -2     & 1      & 0      & \cdots &   0\\
0      & \ddots & \ddots & \ddots & \ddots & \vdots\\
\vdots & \ddots & \ddots & \ddots & \ddots & 0\\
0      & \cdots & 0      & 1      &-2      & 1\\
0      & \cdots & 0      & 0      &1       & -2
\end{bmatrix}.
\]
Quindi, la discretizzazione del nostro problema differenziale diventa
\[ \mathbf{A} \mathbf{u} = \mathbf{b} \]
dove
\[ \mathbf{A} = \mathbf{I}+ \D_1 + \D_2\quad \textrm{e} \quad \mathbf{b}_i = \mathbf{x}_i^3+\mathbf{x}_i-4 \]
per ogni nodo $i$ della discretizzazione del dominio $(0, 1)$.
Tuttavia, il problema cos\`i scritto non tiene (ancora) conto delle condizioni al contorno.\\
Quanto fatto fin'ora può essere riassunto nella seguente porzione di codice (in cui il numero di elementi $\texttt{m}$ in cui dividiamo l'intervallo è già stato definito)
\begin{verbatim}
	x = linspace(0,1,m);
	x = x.';
	h = 1/(m-1);
	D1 = toeplitz(sparse(1,2,-1/(2*h),1,m),sparse(1,2,1/(2*h),1,m));
	D2 = toeplitz(sparse([1,1],[1,2],[-2,1]/(h^2),1,m));
	A = D2 + D1 + speye(m);
	b = x.^3+x-4;
\end{verbatim}
Nel primo caso, considereremo condizioni al bordo di Dirichlet.
Il problema diviene quindi
\begin{equation}\label{eq:exp_lin_dir}
	\begin{cases}
		u(x) + u'(x) + u''(x) =  x^3+x-4 &x\in(0,1)\\
		u(0) =1\\
		u(1)=0
	\end{cases}
\end{equation}
In questo caso l'imposizione delle condizioni al contorno è molto semplice: è sufficiente modificare la prima riga della matrice $\mathbf{A}$ in $[1,0,\ldots,0]$ e il primo elemento di $\mathbf{b}$ in $u(0) = 1$; mentre l'ultima riga di $\mathbf{A}$ deve essere modificata in $[0,\ldots,0,1]$, e l'ultimo elemento di $\mathbf{b}$ in $u(1) = 0$.
Questo procedimento viene eseguito attraverso i comandi
\begin{verbatim}
A(1,1:2) = [1,0];
A(m,m-1:m) = [0,1];
b(1) = 1;
b(m) = 0;
\end{verbatim}
A questo punto è sufficiente risolvere il sistema $\mathbf{A} \mathbf{u} = \mathbf{b}$.

Proviamo ora a risolvere un problema con le condizioni di Neumann:
\[\begin{cases}
u(x) + u'(x) + u''(x) =  x^3+x-4 &x\in(0,1)\\
u'(0) =1\\
u'(1)=-2
\end{cases}\]
In questo caso l'imposizione delle condizioni al bordo è \emph{leggermente} più complicata. Chiamiamo
\[ \bar u = u'(0)\quad\textrm{e}\quad \tilde u = u'(1).\]
Abbiamo allora le seguenti discretizzazioni della derivata al primo e ultimo nodo (utilizzando due nodi fantasma $u_0$ ed $u_{m+1}$)
\[ u_1 + \frac{u_2-u_0}{2h} + \frac{u_0-2u_1+u_2}{h^2} = x_1^3+x_1-4 \]
\[ u_m + \frac{u_{m+1}-u_{m-1}}{2h} + \frac{u_{m-1}-2u_m+u_{m+1}}{h^2} = x_m^3+x_m-4 \]
inserendo le seguenti espressioni per i nodi fantasma
\[ u_0 = u_2-2h\bar{u} \qquad u_{m+1} = u_{m-1}+2h\tilde{u} \]
otteniamo, come prima e ultima riga del sistema da risolvere
\begin{align*}
	u_1\left(1-\frac{2}{h^2}\right) + u_2\,\frac{2}{h^2} & = -5+\frac{2}{h} \\
	u_m\left( 1-\frac{2}{h^2} \right) + u_{m-1}\,\frac{2}{h^2} & = \frac{4}{h}
\end{align*}
A titolo d'esempio, vediamo i passaggi per l'estremo sinistro:
\begin{align*}
	&u_1 + \frac{u_2-u_0}{2h}+\frac{u_0-2u_1+u_2}{h^2} = x_1^3+x_1-4\\
	&\qquad\qquad\Leftrightarrow u1 + \frac{u_2-u_2+2h}{2h} + \frac{u_2-2h-2u_1+u_2}{h^2} = -4\\
	&\qquad\qquad\Leftrightarrow u_1 + \frac{2h}{2h} + \frac{2u_2-2u_1-2h}{h^2} = -4\\
	&\qquad\qquad\Leftrightarrow u_1\left(1-\frac{2}{h^2}\right) + u_2\left( \frac{2}{h^2} \right) = -5+\frac{2}{h}
\end{align*}
Queste condizioni vengono applicate coi seguenti comandi:
\begin{verbatim}
	A(1,1:2) = [1-2/h^2,2/h^2];
	A(m,m-1:m) = [2/h^2,1-2/h^2];
	b(1) = -5+2/h;
	b(m) = 4/h;
\end{verbatim}
Infine, consideriamo anche un problema con condizioni di Robin (all'estremo sinistro del dominio):
\[\begin{cases}
	u(x) + u'(x) + u''(x) = x^3+x-4 &x\in(0,1)\\
	u'(0) - u(0) =0\\
	u(1)=0
\end{cases}\]
Ovviamente la condizione all'estremo destro è uguale al primo esempio \eqref{eq:exp_lin_dir}. Vediamo invece come 
applicare la condizione di Robin ($u_1=\bar{u}$). La prima riga del sistema è
\[ u_1 + \frac{u_2-u_0}{2h} + \frac{u_0-2u_1+u_2}{h^2} = x_1^3+x_1-4 \]
sostituendo 
\[ u_0 = u_2-2h\bar{u} = u_2-2hu_1 \]
otteniamo
\[ u_1\left( 2-\frac{2}{h}-\frac{2}{h^2} \right) + u_2\frac{2}{h^2} = -4 \]
e dobbiamo quindi applicare le seguenti modifiche
\begin{verbatim}
	A(1,1:2) = [2-2/h-2/h^2,2/h^2];
	A(m,m-1:m) = [0,1];
	b(1) = -4;
	b(m) = 0;
\end{verbatim}
\begin{figure}[htb]
	\centering
	\includegraphics[width=0.8\linewidth]{imgs/FD_multi_plot.eps}
	\caption{Risultati ottenuti nell'approssimazione del problema differenziale $u(x) + u'(x) + u''(x) =  x^3+x-4 $ con tre diverse condizioni al contorno.
	Si può notare come in tutti i casi abbiamo convergenza quadratica.}
	\label{fig:bvp1}
\end{figure}
In Figura \ref{fig:bvp1} si nota l'ordine di convergenza dei tre ``diversi'' problemi.

Potrebbero capitare casi in cui la linearit\`a del problema sia ``meno intuitiva''. Ad esempio nel seguente problema (che ha come soluzione esatta $u(x) = e^{x^2}$)
\[\begin{cases}
	\tfrac{u''(x)}{4x^2+2} - u(x) = 0& x\in(0,1)\\
	u(0)=1\\
	u(1)=e
\end{cases}\]
il ``coefficiente'' di $u''$ non è una costante ma una certa funzione di $x$. Il problema \`e comunque lineare in $u$, che \`e la nostra incognita! In questo caso la versione discretizzata del problema in riga $i$ è
\[ \frac{1}{4x_i^2+2}\left(\frac{u_{i+1}-2u_i+u_{i-1}}{h^2}\right) - u_i = 0 \]
quindi possiamo scrivere il problema in forma lineare come
\[ \left[
	\begin{array}{c|ccc|c}
		1      & 0                               & \cdots                           & 0                               & 0\\\hline
		0      &                                 &                                  &                                 & 0\\
		\vdots & \frac{1}{h^2}\frac{1}{4x_i^2+2} & \frac{-2}{h^2}\frac{1}{4x_i^2+2} & \frac{1}{h^2}\frac{1}{4x_i^2+2} & \vdots\\
		0      &                                 &                                  &                                 & 0\\\hline
		0      & 0                               & \cdots                           & 0                               &1\\
	\end{array}
	\right] - \left[
	\begin{array}{c|ccc|c}
		0	     & \cdots	& \cdots    & \cdots	&	0	\\		\hline
		\vdots & 	      &           &         &\vdots\\
		\vdots &        & \tilde{I} &		      &\vdots\\
		\vdots &        &           &         &\vdots\\		\hline
		0	     & \cdots	& \cdots    & \cdots  & 0	\\
	\end{array}\right] =
	\begin{bmatrix}
		1\\
		0\\
		\vdots\\
		0\\
		\ee
	\end{bmatrix}
\]
dove $\tilde{I}$ è la matrice identità di misura $(m-2)\times(m-2)$. La risoluzione del problema si implementa come segue:
\begin{verbatim}
	x = linspace(0,1,m);
	x = x';
	xx = 1./(4*x.^2+2);
	A = spdiags([[xx(2:m-1);0;0],[0;-2*xx(2:m-1);0],[0;0;xx(2:m-1)]],...
		[-1,0,1],m,m)/(h^2);
	A = A - speye(m);
	A(1,1) = 1;
	A(m,m) = 1;
	b = zeros(m,1);
	b(1) = 1;
	b(m) = exp(1);
	u = A\b;
\end{verbatim}

\section{Caso non lineare}
Vedremo adesso qualche esempio nel caso non lineare, e vedremo anche come verificare il corretto ordine di convergenza pur non conoscendo la soluzione analitica.\\
Iniziamo studiando il seguente problema:
\begin{equation}
	\begin{cases}
	u''(x)+\frac{1}{8}u(x)u'(x)=4+\frac{1}{4}x^3& x\in(1,3)\\
	u(1)=17\\
	u(3)=\frac{43}{3}
	\end{cases}\label{eq:nl1}
\end{equation}
che ha come soluzione esatta
\[ \hat{u}(x) = x^2+\frac{16}{x}. \]
Vogliamo implementare un metodo di Newton: la funzione che vogliamo azzerare è
\begin{equation}
	F(u) =
	\begin{bmatrix}
		u_1\\\hline
		\vdots\\
		\frac{u_{i+1}-2u_i+u_{i-1}}{h^2}+\frac{1}{8}\left(\frac{u_{i+1}-u_{i-1}}{2h}\right)u_i\\
		\vdots\\\hline
		u_m
	\end{bmatrix}
	-
	\begin{bmatrix}
		17\\\hline
		\vdots\\
		4+\frac{1}{4}x_i^3\\
		\vdots\\\hline
		\frac{43}{3}
	\end{bmatrix}
\end{equation}
e può essere implementata come segue
\begin{verbatim}
b = 4+x.^3/4;
b(1) = 17;
b(m) = 43/3;
F = @(u) [u(1);(D2*u+1/8*((D1*u).*u))(2:m-1);u(m)] - b;
\end{verbatim}
Se denotiamo con $\tilde{\D_2}$ la matrice $\D_2$ privata di prima e ultima riga, otteniamo che possiamo scrivere la matrice Jacobiana come
 $$J(u) = \begin{bmatrix}
 1&0&\cdots&\cdots&0\\\hline
 &&&&\\
 &&\tilde{\D_2}&&\\
  &&&&\\\hline
   0&\cdots&\cdots&0&1\\
\end{bmatrix}+\frac{1}{16h}
\begin{bmatrix}
 0&\cdots&\cdots&\cdots&0\\\hline
 &&&&\\
 &-u_i&u_{i+1}-u_{i-1}&u_i&\\
  &&&&\\\hline
   0&\cdots&\cdots&\cdots&0\\
\end{bmatrix}$$
che viene implementata come segue:
\begin{verbatim}
D2(1,1:2) = [1,0];
D2(m,m-1:m) = [0,1];
Jfun = @(u) D2 + spdiags([[-u(2:m-1);0;0],[0;u(3:m)-u(1:m-2);0],...
  [0;0;u(2:m-1)]],[-1,0,1],m,m)/(16*h);
\end{verbatim}
A questo punto prendiamo come guess iniziale una linea dal $17$ a $\frac{43}{3}$ ed applichiamo il metodo di Newton esatto
\begin{verbatim}
tol = 1e-12;
maxit = 50;
it = 0;
u = linspace(17,43/3,m);
u = u';
res = -Jfun(u)\F(u);
while ((norm(res,inf)>tol) && (it<maxit))
  u = u+res;
  res = -Jfun(u)\F(u);
  it = it+1;
end
\end{verbatim}
ed anche il metodo di Newton inesatto
\begin{verbatim}
it = 0;
u = linspace(17,43/3,m);
u = u';
JF = Jfun(u);
res = -JF\F(u);
while ((norm(res,inf)>tol) && (it<maxit))
  u = u+res;
  res = -JF\F(u);
  it = it+1;
end
\end{verbatim}
I due metodi danno la stessa convergenza della soluzione (come si vede in Figura \ref{fig:bvpnl1}), l'unica differenza sta nel fatto che il metodo esatto usa 4 iterazioni per convergere, mentre quello inesatto ne usa 13.

\begin{figure}[htb]\centering
\includegraphics[width=0.8\linewidth]{imgs/FD_NL_1_plot.eps}
\label{fig:bvpnl1}\end{figure}

Vediamo ora come implementare il metodo di Newton nel (leggermente) più complicato caso delle condizioni di Neumann. Si consideri il seguente problema:
$$\begin{cases}
u(x)u'(x) + \frac{u''(x)}{2} =0& x\in(1,2)\\
u(1)=1\\
u'(2)=-\frac{1}{4}
\end{cases}$$
Iniziamo vedendo come implementare le condizioni di Neumann: il problema discretizzato si scrive, in ultima riga,
\begin{equation}
u_m\left(\frac{u_{m+1}-u_{m-1}}{2h}\right)+\frac{1}{2}\left(\frac{u_{m+1}-2u_m+u_{m-1}}{h^2}\right)=0
\label{eq:aprx1}
\end{equation}
approssimando $u_{m+1}$ con
$$u_{m+1} = u_{m-1}+2h\,u'(2) = u_{m-1}+2h\frac{-1}{4}=u_{m-1}-\frac{1}{2}h$$
otteniamo che (\ref{eq:aprx1}) si riscrive come
$$u_m\left(-\frac{1}{4}-\frac{1}{h^2}\right) + u_{m-1}\left(\frac{1}{h^2}\right) - \frac{1}{4h}=0.$$
Abbiamo quindi che la nostra funzione da azzerare è
$$F(u)=\begin{bmatrix}
u_1\\\hline
\vdots\\
u_i\left(\frac{u_{i+1}-u_{i-1}}{2h}\right) + 2\left(\frac{u_{i+1}-2u_i+u_{i-1}}{h^2}\right)\\
\vdots\\\hline
u_m\left(-\frac{1}{4}-\frac{1}{h^2}\right) + u_{m-1}\left(\frac{1}{h^2}\right)
\end{bmatrix} -
\begin{bmatrix}
1\\\hline
\vdots\\
0\\
\vdots\\\hline
\frac{1}{4h}
\end{bmatrix}$$
che ha, come Jacobiano
$$J(u)=\begin{bmatrix}
0&\cdots&\cdots&\cdots&0\\\hline
\\
&-u_i&u_{i+1}-u_{i-1}&u_i&  \\
\\\hline
0&\cdots&\cdots&\cdots&0
\end{bmatrix}\frac{1}{2h} +
\begin{bmatrix}
1&0&\cdots&\cdots&0\\\hline
&&& \\
&&\tilde{\D_2}&&  \\
&&&& \\\hline
0&\cdots&0&\frac{1}{h^2}&-\frac{1}{4}-\frac{1}{h^2}
\end{bmatrix}$$
La funzione e lo Jacobiano si implementano come segue:
\begin{verbatim}
F = @(u) [u(1);(u.*(D1*u)+0.5*D2*u)(2:m-1);u(m)*(-1/4-1/h^2) + ...
  u(m-1)/h^2] - b;
D2(1,1:2) = [1,0]*2;
D2(m,m-1:m) = [1/h^2,-1/4-1/h^2]*2;
Jfun = @(u) 0.5*D2 + spdiags([[-u(2:m-1);0;0],[0;u(3:m)-u(1:m-2);0],...
  [0;0;u (2:m-1)]],[-1,0,1],m,m)/(2*h);
\end{verbatim}
A questo punto è sufficiente implementare il metodo di Newton.

Nel caso in cui non sia possibile avere la soluzione esatta, il miglior modo per verificare l'ordine di convergenza è stimare prima una soluzione su una griglia molto fine, ed utilizzarla come soluzione esatta.
Per fare ciò, bisogna stare attenti a scegliere il numero giusto di intervalli in cui dividere l'intervallo: la soluzione migliore (o, perlomeno, quella che trovo pi\`u comoda) è usare $2^k + 1$  nodi, con $k\in\mathbb{N}$.
In questo modo le varie ``soluzioni'' si interpolano nei punti giusti, come si nota nel seguente schema:
\begin{center}
\begin{tikzpicture}
\fill (0,-1) circle (0.05);
\fill (0.25,-1) circle (0.05);
\fill (0.5,-1) circle (0.05);
\fill (0.75,-1) circle (0.05);
\fill (1,-1) circle (0.05);
\fill (1.25,-1) circle (0.05);
\fill (1.5,-1) circle (0.05);
\fill (1.75,-1) circle (0.05);
\fill (2,-1) circle (0.05);
\fill (2.25,-1) circle (0.05);
\fill (2.5,-1) circle (0.05);
\fill (2.75,-1) circle (0.05);
\fill (3,-1) circle (0.05);
\fill (3.25,-1) circle (0.05);
\fill (3.5,-1) circle (0.05);
\fill (3.75,-1) circle (0.05);
\fill (4,-1) circle (0.05);
\node at (5,-1) {$k=4$};
\fill (0,0) circle (0.05);
\fill (0.5,0) circle (0.05);
\fill (1,0) circle (0.05);
\fill (1.5,0) circle (0.05);
\fill (2,0) circle (0.05);
\fill (2.5,0) circle (0.05);
\fill (3,0) circle (0.05);
\fill (3.5,0) circle (0.05);
\fill (4,0) circle (0.05);
\node at (5,0) {$k=3$};
\fill (0,1) circle (0.05);
\fill (1,1) circle (0.05);
\fill (2,1) circle (0.05);
\fill (3,1) circle (0.05);
\fill (4,1) circle (0.05);
\node at (5,1) {$k=2$};
\fill (0,2) circle (0.05);
\fill (2,2) circle (0.05);
\fill (4,2) circle (0.05);
\node at (5,2) {$k=1$};
\end{tikzpicture}
\end{center}
Quindi se la soluzione di riferimento $U$ viene stimata con $M$ nodi, e quella approssimata $u$ con 
$m$, abbiamo che, per interpolare correttamente, stimeremo l'errore come segue
\begin{verbatim}
err = norm(u-U(1:(M-1)/(m-1):M),inf);
\end{verbatim}
A titolo d'esempio, consideriamo il problema
$$\begin{cases}
u''(x)+\sin(u'(x))+u(x) =x& x\in(0,1)\\
u(0)=0\\
u(1)=1\end{cases}
$$
Questo problema si implementa con la funzione
$$F(u)=\begin{bmatrix}
u_1\\\hline
\vdots\\
\frac{u_{i+1}-2u_i+u_{i-1}}{h^2} + \sin\left(\frac{u_{i+1}-u_{i-1}}{2h}\right)+u_i - x_i\\
\vdots\\\hline
u_m-1
\end{bmatrix}$$
che ha come Jacobiano
$$J(u)=\begin{bmatrix}
0&0&\cdots&0&0\\\hline
&&&&\\
&&\tilde{D2}&&\\
&&&&\\\hline
0&0&\cdots&0&0\end{bmatrix}
+I+\begin{bmatrix}
0&\cdots&\cdots&\cdots&0\\\hline
&&&&\\
&-\cos\left(\frac{u_{i+1}-u_{i-1}}{2h}\right)&0&\cos\left(\frac{u_{i+1}-u_{i-1}}{2h}\right)&\\
&&&&\\\hline
0&\cdots&\cdots&\cdots&0
\end{bmatrix}\frac{1}{2h}$$
implementate come segue
\begin{verbatim}
b = linspace(0,1,m);
b = b';
b(1) = 0;
b(m) = 1;
F = @(u) [u(1);(D2*u+sin(D1*u)+u)(2:m-1);u(m)] - b;
D2(1,1:2) = [0,0];
D2(m,m-1:m) = [0,0];
Jfun = @(u) D2 + speye(m) + spdiags([[-cos((u(3:m)-u(1:m-2))/(2*h));0;0],...
  [0;0;cos((u(3:m)-u(1:m-2))/(2*h))]],[-1,1],m,m)/(2*h);
\end{verbatim}
\section{Nodi non equispaziati}
Se denotiamo con $h_i$
$$h_i = x_{i+1}-x_i$$
abbiamo che la riga $i$ di $D1$ ha, in colonna $i-1$ ed $i+1$ rispettivamente
\[ -\frac{1}{h_{i-1} + h_{i}},\quad \frac{1}{h_{i-1} + h_{i}} ,\]
mentre la riga $i$ di $D2$, in colonna $i-1,i,i+1$ avr\`a rispettivamente
\[ \frac{2}{h_{i-1}(h_{i-1} h_{i})},\quad \frac{-2}{h_{i-1}h_i}, \quad \frac{2}{h_{i-1}(h_{i-1} h_{i})} . \]
