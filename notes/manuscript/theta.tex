\section{Eulero esplicito}
Il primo metodo che vedremo è il cosiddetto \emph{Eulero esplicito}: denotiamo con $k$ il passo tempolare, allora il metodo si riassume nella seguente formula
$$\y_{n+1} = \y_{n}+ kf(\y_{n})$$
dove $\y_{n+1}$rappresenta il passo che dobbiamo ancora calcolare, $\y_{n}$ l'ultimo passo calcolato.\\
Come primo esempio vediamo un problema omogeneo del primo ordine:
\begin{equation}\begin{cases}
y'=5y-3 \\
y(2) = 1
\end{cases}\label{sist:ode1}\end{equation}
che ha come soluzione 
$$y(t) = \frac{2}{5}e^{5(t-2)}+\frac{3}{5}$$
In questo caso l'implementazione è molto semplice e viene implementata con i seguenti comandi
\begin{verbatim}
t = linspace(2,3,m);
k = 1/(m-1);
y = zeros(1,m);
y(1) = 1;
for n = 1:m-1
  y(n+1) = y(n) + k*(5*y(n)-3);
end
\end{verbatim}
Vediamo un problema lineare di ordine più grande:
\begin{equation}\begin{cases}
y'''=-3y''-3y'-y \\
y(0)=7 \\
y'(0)=-7 \\
y''(0)=11
\end{cases}\label{sist:ode2}\end{equation}
Il problema in forma matriciale può essere scritto nel seguente modo:
$$
\begin{bmatrix}
y\\y'\\y''
\end{bmatrix}' = \begin{bmatrix}
0&1&0\\0&0&1\\-1&-3&-3
\end{bmatrix}\begin{bmatrix}
y\\y'\\y''
\end{bmatrix}
$$
Quindi ad ogni iterazione calcoleremo $\y_{n+1}$ come segue:
$$\y_{n+1} = \y_{n} + k\begin{bmatrix}
0&1&0\\0&0&1\\-1&-3&-3\end{bmatrix} \y_{n}$$
L'implementazione è la seguente:
\begin{verbatim}
k = 1/(m-1);
y(:,1) = [7;-7;11];
A = k*[0,1,0;0,0,1;-1,-3,-3];
for n=1:m-1
  y(:,n+1) = y(:,n) + A*y(:,n);
end
\end{verbatim}
Passiamo ad un problema del primo ordine non omogeneo:
\begin{equation}\begin{cases}
y' = -3y + \sin(2t)\\
y(\pi) = 0
\end{cases}\label{sist:ode3}\end{equation}
che ha come soluzione esatta
$$y(t) = \frac{3}{13}\sin(2t)-\frac{2}{13}\cos(2t)+\frac{2}{13} e^{3\pi-3t}$$
In questo caso abbiamo la seguente versione discretizzata del problema:
$$\begin{bmatrix}
y\\t
\end{bmatrix}' = 
\begin{bmatrix}
-3 y + \sin(2 t)\\
1
\end{bmatrix}$$
con dato iniziale
$$ \begin{bmatrix}
y_0\\t_0
\end{bmatrix} = \begin{bmatrix}
0\\ \pi
\end{bmatrix}$$
E si implementa quindi come segue:
\begin{verbatim}
t = linspace(pi,2*pi,m);
k = pi/(m-1);
f = @(y) [-3*y(1)+sin(2*y(2));1];
y = zeros(2,m);
y(:,1) = [0;pi];
for n = 1:m-1
  y(:,n+1) = y(:,n) + k*feval(f,(y(:,n)));
end
\end{verbatim}
Consideriamo infine un problema del terzo ordine non omogeneo:
\begin{equation}
\begin{cases}
y''' = -y''-4y'-2y+2\cos^2(t) \\
y(0) = 0\\
y'(0) = 0\\
y''(0) = 2
\end{cases}
\label{sist:ode4}\end{equation}
che ha come soluzione esatta
$$y = \sin^2(t)$$
Quindi il nostro problema in forma discretizzata diventa:
$$\begin{bmatrix}
y\\ y'\\ y''\\t
\end{bmatrix}'=
\begin{bmatrix}
y'\\
y''\\
-y''-4y'-2y+2\cos^2(t)\\
1
\end{bmatrix}
$$
con dato iniziale
$$\begin{bmatrix}
0\\0\\2\\0
\end{bmatrix}
$$
La soluzione si implementa come segue:
\begin{verbatim}
t = linspace(0,1,m);
k = 1/(m-1);
f = @(y) [y(2);y(3);-y(3)-4*y(2)-2*y(1)+2*cos(y(4))^2;1];
y = zeros(4,m);
y(:,1) = [0;0;2;0];
for n = 1:m-1
  y(:,n+1) = y(:,n) + k*feval(f,(y(:,n)));
end
\end{verbatim}
I risultati di questa implementazione sono mostrati in Figura \ref{fig:ode1}
\begin{figure}[tb]
\centering
\includegraphics[width=0.8\linewidth]{imgs/Eul_esp_plot.eps}
\caption{Risultati nell'approssimazione dei problemi (\ref{sist:ode1}),(\ref{sist:ode2}), (\ref{sist:ode3}), e (\ref{sist:ode4}) mediante il metodo Eulero esplicito. Si noti la convergenza di ordine $1$.}
\label{fig:ode1}\end{figure}

\section{Metodo dei trapezi}
Denotiamo con $\y_{n}$ l'ultima approssimazione già calcolata e $\y_{n+1}$ la soluzione (da approssimare) al passo successivo. Allora il metodo dei trapezi si riassume nella formula 
$$ \y_{n+1} = \y_{n} + \frac{k}{2}\big(f(\y_{n},t_n)+f(\y_{n+1},t_{n+1})\big) $$
Il metodo si traduce nel risolvere
$$F(\y_{n},\y_{n+1}) \doteq \y_{n+1} - \frac{k}{2} f(\y_{n+1},t_{n+1}) - \y_{n} - \frac{k}{2}f(\y_{n},t_n) = 0$$
Si tratta di un metodo implicito, ma questo non significa che sia sempre necessario utilizzare il metodo di Newton! Ad esempio nel caso del problema (\ref{sist:ode1}) 
$$ f(y) = 5y-3 $$
quindi
$$F(\y_{n+1}) = \y_{n+1} - \frac{k}{2}(5\y_{n+1}-3+5\y_{n}-3) -\y_{n}$$
poiché conosciamo il valore $\y_{n}$, abbiamo che la soluzione al passo successivo è 
$$\y_{n+1} = \frac{\y_{n}\left(1+\frac{5}{2}k\right)-3k}{1-\frac{5}{2}k}$$
e l'implementazione si scrive come segue:
\begin{verbatim}
for n = 1:m-1
  y(n+1) = (y(n)*(1+5*k/2)-3*k)/(1-5*k/2);
end
\end{verbatim}
Vediamo ora il metodo dei trapezi applicato al problema (\ref{sist:ode2}). Iniziamo chiamando $A$ la matrice
$$A = \begin{bmatrix}
0&1&0\\0&0&1\\-1&-3&-3
\end{bmatrix}$$
Il metodo dei trapezi ci chede quindi di risolvere, ad ogni passo,
$$\y_{n+1} -\frac{k}{2}A\y_{n+1} - \frac{k}{2}A\y_{n} - \y_{n} = 0$$
ovvero
$$\left(I-\frac{k}{2}A\right)\y_{n+1} = \left(I+\frac{k}{2}A\right)\y_{n}$$
dove $\y_{n+1}$ è l'incognita, mentre $\y_{n}$ è noto. Il metodo si implementa come segue
\begin{verbatim}
y(:,1) = [7;-7;11];
A = k/2*[0,1,0;0,0,1;-1,-3,-3];
for n=1:m-1
  y(:,n+1) = (eye(3)-A)\((eye(3)+A)*y(:,n));
end
\end{verbatim}
Consideriamo ora il problema (\ref{sist:ode3}). La funzione da azzerare in questo caso è
$$
F\left(\begin{bmatrix}
y_{n+1}\\t_{n+1}
\end{bmatrix}\right) = 
\begin{bmatrix}
y_{n+1}\\t_{n+1}
\end{bmatrix} - \frac{k}{2}\left(\begin{bmatrix}
-3y_{n+1} + \sin(2t_{n+1})\\1
\end{bmatrix} + \begin{bmatrix}
-3y_{n} + \sin(2t_{n})\\1
\end{bmatrix}\right) - \begin{bmatrix}
y_{n}\\t_{n}
\end{bmatrix}$$
Potrebbe sembrare necessario l'utilizzo del metodo di Newton, ma così non è poiché l'unica parte non lineare è una funzione di $t$, di cui conosciamo il valore ad ogni step. La funzione da azzerare è quindi
$$F(y_{n+1}) = y_{n+1} - \frac{k}{2}\big( -3y_{n+1}+\sin(2t_{n+1}) - 3y_{n} + \sin(2t_n) \big)-y_{n}$$
che ha valore zero per
$$y_{n+1} = -\frac{\frac{k}{2}(-\sin(2t_{n+1})+3y_{n}-\sin(2t_n))}{1+\frac{3k}{2}}-y_{n}$$
e si implementa come segue:
\begin{verbatim}
t = linspace(pi,2*pi,m);
k = pi/(m-1);
y = zeros(1,m);
y(1) = 0;
for n = 1:m-1
  y(n+1) = -(k/2*(-sin(2*t(n+1))+3*y(:,n)-sin(2*t(n))) - y(:,n))/(1+3*k/2);
end
\end{verbatim}
Vediamo come risolvere numericamente il problema (\ref{sist:ode4}). Dobbiamo trovare lo zero della seguente funzione
\begin{multline*}F\left(y_{n+1}\right) = 
\begin{bmatrix}\\ \y_{n+1}\\\,\end{bmatrix} - \frac{k}{2}\left(\begin{bmatrix}
0&1&0\\0&0&1\\-2&-4&-1
\end{bmatrix}\begin{bmatrix}\\ \y_{n+1}\\\,\end{bmatrix}+ \begin{bmatrix}
0\\0\\2\cos^2(t_{n+1})
\end{bmatrix}\right)+\ldots\\ -\begin{bmatrix}\\ \y_n\\\,\end{bmatrix} -\frac{k}{2}\left(\begin{bmatrix}
0&1&0\\0&0&1\\-2&-4&-1
\end{bmatrix}\begin{bmatrix}\\ \y_n\\\,\end{bmatrix}+ \begin{bmatrix}
0\\0\\2\cos^2(t_n)
\end{bmatrix}\right)
\end{multline*}
Se chiamiamo $A$ la matrice
$$A = \begin{bmatrix}
0&1&0\\0&0&1\\-2&-4&-1
\end{bmatrix}$$
il problema si traduce nel risolvere il seguente sistema
$$-\left(I-\frac{k}{2}A\right)\begin{bmatrix}\\ \y_{n+1}\\\,\end{bmatrix} = -k\begin{bmatrix}0\\0\\ \cos^2(t_N)\end{bmatrix}-\begin{bmatrix}\\ \y_n\\\,\end{bmatrix}-k\begin{bmatrix}0\\0\\ \cos^2(t_O)\end{bmatrix}-\frac{k}{2}A\begin{bmatrix}\\ \y_n\\\,\end{bmatrix}
$$
E si implementa come segue:
\begin{verbatim}
t = linspace(0,1,m);
k = 1/(m-1);
y = zeros(3,m);
y(:,1) = [0;0;2];
A = [0,1,0;0,0,1;-2,-4,-1];
for n = 1:m-1
  b = -k*[0;0;cos(t(n+1))^2] - y(:,n)-k*[0;0;cos(t(n))^2] - k/2*A*y(:,n);
  B = -(eye(3)-k/2*A);
  y(:,n+1) = B\b;
end
\end{verbatim}
I risultati di queste approssimazioni sono riportate in Figura \ref{fig:ode2}
\begin{figure}[tb]
\centering
\includegraphics[width=0.8\linewidth]{imgs/Trap_plot.eps}
\caption{Risultati nell'approssimazione dei problemi (\ref{sist:ode1}),(\ref{sist:ode2}), (\ref{sist:ode3}), e (\ref{sist:ode4}) mediante il metodo dei trapezi. Si noti la convergenza di ordine $2$.}
\label{fig:ode2}\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Un problema non lineare}
Vediamo ora come applicare il metodo di Eulero ed il metodo dei trapezi a un problema non lineare. Si consideri il seguente problema
\begin{equation}
\begin{cases}
y'''=96\frac{y}{y'}\\
y(1) = 1\\
y'(1) = 4\\
y''(1) = 12
\end{cases}
\label{sist:ode_nl_1}\end{equation}
L'implementazione del metodo di Eulero è molto semplice:
\begin{verbatim}
y_E = zeros(3,m);
y_E(:,1) = [1;4;12];
f = @(y) [y(2);y(3);96*y(1)/y(2)];
for n = 1:m-1
  y_E(:,n+1) = y_E(:,n) + k*f(y_E(:,n));
end
\end{verbatim}
Il problema si traduce, per il metodo dei trapezi,nell'azzerare
$$F\left(\begin{bmatrix}\\ \y_{n+1}\\\,\end{bmatrix}\right) = 
\begin{bmatrix}\\ \y_{n+1}\\\,\end{bmatrix} - \frac{k}{2}\begin{bmatrix} \y_{n+1}^{(2)}\\ \y_{n+1}^{(3)}\\96\frac{ \y_{n+1}^{(1)}}{ \y_{n+1}^{(2)}}\end{bmatrix} -\begin{bmatrix}\\ \y_{n}\\\,\end{bmatrix} - \frac{k}{2}\begin{bmatrix} \y_{n}^{(2)}\\ \y_{n}^{(3)}\\96\frac{ \y_{n}^{(1)}}{ \y_{n}^{(2)}}\end{bmatrix}$$
In questo caso si rende necessario il metodo di Newton. Calcoliamo quindi lo Jacobiano
$$J = I - \frac{k}{2}\begin{bmatrix}
0&1&0\\
0&0&1\\
96\frac{1}{ \y_{n+1}^{(2)}} & -96\frac{ \y_{n+1}^{(1)}}{ \left(\y_{n+1}^{(2)}\right)^2}&0
\end{bmatrix}$$
Il metodo si implementa come segue:
\begin{verbatim}
F = @(yo,yn) yn - k/2*f(yn) - yo - k/2*f(yo);
JF = @(yn) eye(3) -k/2*[0,1,0;0,0,1;96/yn(2),-96*yn(1)/(yn(2)^2),0];
y_T = zeros(3,m);
y_T(:,1) = [1;4;12];
maxit = 30;
tol = 1e-10;
for n = 1:m-1
  y_T(:,n+1) = y_T(:,n);
  res = -JF(y_T(:,n+1))\F(y_T(:,n),y_T(:,n+1));
  it = 0;
  while it<maxit && norm(res)>tol
    y_T(:,n+1) = y_T(:,n+1) + res;
    res = -JF(y_T(:,n+1))\F(y_T(:,n),y_T(:,n+1));
    it = it+1;
  end
end
\end{verbatim}
In Figura \ref{fig:ode_nl_1} si possono osservere i risultati ottenuti con questi metodi
\begin{figure}[tb]
\centering
\includegraphics[width=0.8\linewidth]{imgs/NL_eul_plot.eps}
\caption{Risultati nell'approssimazione del problema (\ref{sist:ode_nl_1}) mediante il metodo di Eulero ed il metodo dei trapezi.}
\label{fig:ode_nl_1}\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Una generalizzazione: il $\theta$-metodo}
Il $\theta$-metodo si riassume nella seguente formula
$$ \y_{n+1} = \y_n + k(1-\theta)f(\y_n,t_n) + k\theta f(\y_{n+1},t_{n+1})\quad \theta\in[0,1]$$
o, equivalentemente, nell'azzerare la funzione
$$F( \y_{n+1}) = \y_{n+1} - \y_n -k(1-\theta)f(\y_n,t_n) - k\theta f(\y_{n+1},t_{n+1})$$
Prendiamo come esempio il problema (\ref{sist:ode4}). La formula del $\theta$-metodo diventa
$$y_{n+1} = \y_n + k(1-\theta)(A\y_n + T_n) + k\theta (A\y_{n+1}+T_{n+1})$$
con
$$A = \begin{bmatrix}0&1&0\\0&0&1\\-2&-4&-1\end{bmatrix}\qquad T_n = \begin{bmatrix}0\\0\\\cos^2(t_n) \end{bmatrix}$$
quindi
$$(I-k\theta A)\y_{n+1} + (I+k(1-\theta)A)\y_n + k\theta T_{n+1} + (1-k\theta)T_n$$
Si noti che per $\theta=0$ abbiamo il metodo di Eulero, per $\theta=0.5$ abbiamo il metodo dei trapezi, mentre per $\theta=1$ abbiamo il metodo di Eulero implicito.
Il metodo viene implementato come segue
\begin{verbatim}
for n = 1:m-1
  b = k*theta*[2*cos(t(n+1))^2;0;0] + (1-theta)*k*[2*cos(t(n))^2;0;0] +...
    (eye(3)+k*(1-theta)*A)*y(:,n);
  B = eye(3)-k*theta*A;
  y(:,n+1) = B\b;
end
\end{verbatim}
In Figura \ref{fig:ode_theta} si vede l'ordine di convergenza per diversi valori di $\theta$.
\begin{figure}[tb]
\centering
\includegraphics[width=0.8\linewidth]{imgs/Theta_plot.eps}
\caption{Risultati nell'approssimazione del problema (\ref{sist:ode4}) mediante il $\theta$-metodo: in legenda vengono riportati i vari valori di $\theta$. Si noti che sono tutti di ordine $1$, tranne che per il valore $0.5$ (metodo dei trapezi).}
\label{fig:ode_theta}\end{figure}
