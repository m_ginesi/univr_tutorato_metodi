\section{Metodi Runge-Kutta}
Nella forma più generale, un metodo RK può essere scritto come
\begin{subequations}
\label{eqs:RK_formulazione_1}
\begin{equation}
\y_{n+1} = \y_n + kF(t_n,\y_n,h;f),\quad n\geq0
\end{equation}
dove $F$ è la funzione incremento
\begin{equation}
F(t_n,\y_n,h;f) = \sum_{i=1}^s b_iK_i
\end{equation}
dove i coefficienti $K_i$ sono dati da
\begin{equation} 
K_i = f\left(t_n+c_ih,\y_n + h\sum_{j=1}^s a_{ij}K_j\right),
\end{equation}
\end{subequations}
in cui $s$ è il numero di stadi del metodo e i coefficienti $a_{ij},b_i$ ed $c_i$ vengono raccolti nella \emph{matrice di Butcher}
$$\begin{array}{c|cccc}
c_1 & a_{1\,1} & a_{1\,2} & \cdots & a_{1\,s}\\
c_2 & a_{2\,1} & a_{2\,2} & \cdots & a_{2\,s}\\
\vdots&\vdots&\vdots&\ddots&\vdots\\
c_s & a_{s\,1} & a_{s\,2} & \cdots & a_{s\,s}\\\hline
&b_1&b_2&\cdots&b_s
\end{array}
\quad\textrm{o}\quad
\begin{array}{c|c}
\mathbf{c}& A\\\hline\\ &\mathbf{b}^T
\end{array}$$
Equivalenetemente alla formulazione \eqref{eqs:RK_formulazione_1}, si pu\`o scrivere un metodo Runge-Kutta utilizzando la seguente formulazione:
\[ \y_{n+1} = \y_n + k \sum_{j=1}^s b_j f (t_n + kc_j , \xi_j), \]
con
\[ \xi_i = \y_n + k \sum_{j=1}^{s} a_{ij} \f \left( t_n + k c_j , \xi_j \right). \]
\`E tuttavia preferibile utilizzare la formulazione \eqref{eqs:RK_formulazione_1} in quanto permette di utilizzare il metodo evitando di calcolare la funzione $f$ negli stessi punti.

Se $a_{ij}=0$ per $j\geq 0$, per ogni $i=1,2,\ldots,s$, i coefficienti $K_i$ possono essere calcolati esplicitamente e parlaremo allora di \emph{metodo Runge Kutta esplicito}. In caso contrario, il metodo RK è detto \emph{implicito}, in questo caso, per trovare i coefficienti $K_i$ si dovrebbe risolvere un sistema non lineare e sarebbe oneroso; quindi considereremo solo un tipo di RK impliciti, i metdodi \emph{semi-impliciti}, in cui $a_{ij}=0$ se $j>i$ e quindi ogni $K_i$ è dato dalla soluzione dell'equazione non lineare
$$K_i = f\left(t_n + c_ih,\y_n+ha_{ii} K_i+h\sum_{j=1}^{i-1}a_{ij}K_j\right)$$
Supporremo sempre le condizioni
$$c_i = \sum_{j=1}^s a_{ij}\qquad\sum_{i=1}^s b_i = 1.$$
Nel caso di un metodo semi-implicito, l'unica differenza sta nel fatto che per approssimare \verb!K(i)! bisognerà usare, forse ma \emph{non necessariamente}, il metodo di Newton.

Consideriamo il tableau
\begin{equation}
\begin{array}{c|cccc}
0&\\
\frac{1}{2} & \frac{1}{2}\\
\frac12&0&\frac12\\
1&0&0&1\\\hline
&\frac{1}{6}&\frac{1}{3}&\frac{1}{3}&\frac{1}{6}
\end{array}
\label{eq:tableau_esp}\end{equation}
per un metodo esplicito ed il tableau
\begin{equation}
\begin{array}{c|cc}
\frac{3+\sqrt{3}}{6}&\frac{3+\sqrt{3}}{6}\\
\frac{3-\sqrt{3}}{6}&-\frac{\sqrt{3}}{3}&\frac{3+\sqrt{3}}{6}\\\hline
&\frac12&\frac12
\end{array}
\label{eq:tableau_semiimpl}\end{equation}
per un metodo semiimplicito.
Vediamo come utilizzare il metodo dato dal tableau (\ref{eq:tableau_esp}) per il problema (\ref{sist:ode4}). Per prima cosa definiamo i dati
\begin{verbatim}
A = sparse([2,3,4],[1,2,3],[0.5,0.5,1],4,4);
b = [1,2,2,1]/6;
c = [0,1,1,2]/2;
y0 = [0;0;2];
AA = [0,1,0;0,0,1;-2,-4,-1];
f = @(t,y) AA*y + [0;0;2*cos(t)^2];
\end{verbatim}
con questi, risolviamo il problema cose segue
\begin{verbatim}
for n = 1:m-1
  K = zeros(3,4);
  K(:,1) = feval(f,T(n),Y(:,n));
  K(:,2) = feval(f,T(n) + h*c(2),Y(:,n) + h*A(2,1)*K(:,1));
  K(:,3) = feval(f,T(n) + h*c(3),Y(:,n) + ...
    h*(A(3,1)*K(:,1)+A(3,2)*K(:,2)));
  K(:,4) = feval(f,T(n) + h*c(4),Y(:,n) + ...
    h*(A(4,1)*K(:,1)+A(4,2)*K(:,2)+A(4,3)*K(:,3)));
  Y(:,n+1) = Y(:,n) + h*K*b';
endfor
\end{verbatim}
Nel caso invece volessimo usare il tableau (\ref{eq:tableau_semiimpl}), il calcolo dei coefficienti $K_i$ si implementa come segue
\begin{verbatim}
K = zeros(3,2);
K(:,1) = (eye(3)-h*A(1,1)*AA)\(AA*Y(:,n)+[0;0;2*cos(T(n)+h*c(1))^2]);
K(:,2) = (eye(3)-h*A(2,2)*AA)\(AA*(Y(:,n)+h*A(2,1)*K(:,1))+...
  [0;0;2*cos(T(n)+c(2)*h)^2]);
\end{verbatim}
Si osservi che, nonostante il metodo sia (semi)implicito, non è stato necessario utilizzare il metodo di Newton.\\
In Figura \ref{fig:RK} viene mostrato il corretto ordine di convergenza di questi due metodi.
\begin{figure}[tb]
\centering
\includegraphics[width=0.8\linewidth]{imgs/RK_plot.eps}
\caption{Convergenza per il metodo di tableau (\ref{eq:tableau_esp}) e (\ref{eq:tableau_semiimpl}) per la risoluzione del problema (\ref{sist:ode4})}
\label{fig:RK}\end{figure}
